import json

from tinydb import TinyDB

DB = TinyDB("rp_data_db.json")

DB.purge_tables()

#names
names = DB.table("names")

#loacations
locations = DB.table("locations")
inns = DB.table("inns")
times = DB.table("times")
weathers = DB.table("weathers")

#characters
roles = DB.table("roles")
races = DB.table("races")
goals = DB.table("goals")
group_goals = DB.table("group_goals")

#items/spells
items = DB.table("items")
magic_items = DB.table("magic_items")
spells_simple = DB.table("spells_simple")
spells = DB.table("spells")

#extra
meats = DB.table("meats")
voices = DB.table("voices")
animals = DB.table("animals")
contests = DB.table("contests")
bandits = DB.table("bandits")
creatures = DB.table("creatures")
materials = DB.table("materials")
supplies = DB.table("supplies")
colours = DB.table("colours")

hurts = DB.table("hurts")
injuries = DB.table("injuries")
notable_features = DB.table("notable_features")

valuables = DB.table("valuables")
trinkets = DB.table("trinkets")

monsters = DB.table("monsters")

if __name__ == "__main__":
    with open("rp.json", "r", encoding="utf-8") as f:
        old_rp = json.load(f)

    names.insert_multiple([{"str": x, "forename": True} for x in old_rp["names"]["forenames"]])
    names.insert_multiple([{"str": x, "surname": True} for x in old_rp["names"]["surnames"]])

    # locations
    _ = []
    for x in old_rp["locations"]["inside"]:
        x["inside"] = True
        _.append(x)
    for x in old_rp["locations"]["outside"]:
        x["outside"] = True
        _.append(x)
    locations.insert_multiple(_)

    inns.insert_multiple([{"str": x} for x in old_rp["inns"]])
    times.insert_multiple([{"str": t, "index": x} for x, t in enumerate(old_rp["locations"]["times"])])
    weathers.insert_multiple([{"str": w, "index": x} for x, w in enumerate(old_rp["locations"]["weather"])])

    # characters
    roles.insert_multiple([x for x in old_rp["roles"]])
    races.insert_multiple([x for x in old_rp["races"]])
    goals.insert_multiple([{"str": g, "index": x} for x, g in enumerate(old_rp["goals"])])
    group_goals.insert_multiple([{"str": g, "index": x} for x, g in enumerate(old_rp["group_goals"])])

    # items/spells
    items.insert_multiple([{"str": x} for x in old_rp["items"]])
    magic_items.insert_multiple([{"str": x} for x in old_rp["magic"]["items"]])
    spells_simple.insert_multiple([x for x in old_rp["magic"]["spells"]])

    # extra
    meats.insert_multiple([{"str": x} for x in old_rp["meats"]])
    voices.insert_multiple([{"str": x} for x in old_rp["voices"]])
    animals.insert_multiple([{"str": x} for x in old_rp["animals"]])
    contests.insert_multiple([{"str": x} for x in old_rp["contests"]])
    bandits.insert_multiple([{"str": x} for x in old_rp["bandits"]])

    creatures.insert_multiple([{"str": x} for x in old_rp["creatures"]])
    materials.insert_multiple([{"str": x} for x in old_rp["materials"]])
    supplies.insert_multiple([{"str": x} for x in old_rp["supplies"]])
    colours.insert_multiple([{"str": x} for x in old_rp["colour"]])

    hurts.insert_multiple([{"str": x} for x in old_rp["hurt"]])
    injuries.insert_multiple([{"str": x} for x in old_rp["injury"]])
    notable_features.insert_multiple([{"str": x} for x in old_rp["notable_features"]])
    valuables.insert_multiple([{"str": x} for x in old_rp["valuables"]])

    trinkets.insert_multiple([{"str": x} for x in old_rp["trinkets"]])

    monster_keys = ['ac', 'cha', 'trait', 'senses', 'size', 'passive', 'dex', 'immune', 'spells', 'type', 'vulnerable', 'str', 'legendary', 'wis', 'skill', 'description', 'save', 'int', 'con', 'languages', 'speed', 'source', 'conditionImmune', 'alignment', 'hp', 'reaction', 'resist', 'cr', 'action', 'name']
    for m in old_rp["monsters"]:
        for k in monster_keys:
            if k not in m:
                m[k] = "-"
    monsters.insert_multiple(old_rp["monsters"])

    spell_keys = ['text', 'roll', 'classes', 'range', 'school', 'name', 'source', 'components', 'duration', 'time', 'ritual', 'level']
    for s in old_rp["spells"]:
        for k in spell_keys:
            if k not in s:
                s[k] = "-"
        if isinstance(s["text"], str):
            s["text"] = [s["text"]]
    spells.insert_multiple([x for x in old_rp["spells"]])
