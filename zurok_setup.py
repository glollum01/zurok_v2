import os

import yaml


def setup(botname="default"):

    BASE_DIR = os.path.abspath(os.path.dirname(os.path.realpath(__file__)))
    DATA_DIR = os.path.join(BASE_DIR, "data")

    config_options = {
        "token": {
            "type": str,
            "text": "Your bot's token. Needed for your bot to work",
            "required": True,
            "default": ""
        },
        "prefix": {
            "type": str,
            "text": "Your bots command prefix, used to trigger commands",
            "required": False,
            "default": "!"
        },
        "motd": {
            "type": str,
            "text": "Message of the day <not used>",
            "required": False,
            "default": ""
        },
        "info_ch": {
            "type": int,
            "text": "Where your bot posts when it comes online",
            "required": False,
            "default": 0
        },
        "owner": {
            "type": int,
            "text": "You user ID, used to notify you of any errors when using commands",
            "required": False,
            "default": 0
        },
        "devbuild": {
            "type": bool,
            "text": "Is this bot for testing purposes?",
            "required": False,
            "default": False
        },
        "roles": {
            "type": dict,
            "admins": {
                "type": list,
                "text": "User IDs of your bot admins!",
                "required": False,
                "default": []
            },
            "moderators": {
                "type": list,
                "text": "User IDs of your bot mods!",
                "required": False,
                "default": []
            },
            "testers": {
                "type": list,
                "text": "User IDs of your bot testers!",
                "required": False,
                "default": []
            }
        }
    }

    config = {}

    for k in config_options:
        print(k)
        if "text" in config_options[k]:
            print(config_options[k]["text"])
            print(config_options[k]["required"], config_options[k]["type"])
            temp = input(">")
            if temp == "":
                temp = config_options[k]["default"]
            config[k] = temp
        else:
            for k2 in [x for x in config_options[k].keys() if x is not "type"]:
                print(k2)
                print(config_options[k][k2]["text"])
                print(config_options[k][k2]["required"], config_options[k][k2]["type"])
                temp = input(">")
                if temp == "":
                    temp = config_options[k][k2]["default"]
                config[k] = temp


def _key():
    pass


def save(data, botname):
    BASE_DIR = os.path.abspath(os.path.dirname(os.path.realpath(__file__)))
    DATA_DIR = os.path.join(BASE_DIR, "data")
    with open(os.path.join(DATA_DIR, "{}.yaml".format(botname)), "w") as f:
        yaml.dump(data, f, default_flow_style=False)


if __name__ == "__main__":
    setup()
