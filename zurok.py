import logging
import os
import subprocess
import sys
import traceback
from datetime import datetime
from sys import platform

import simpleeval
import yaml
from discord.ext import commands
from discord.ext.commands import errors
from tinydb import TinyDB, Query

simpleeval.MAX_STRING_LENGTH = 1800

if len(sys.argv) > 1:
    BOTNAME = sys.argv[1]
else:
    BOTNAME = "default"

BASE_DIR = os.path.abspath(os.path.dirname(os.path.realpath(__file__)))
DATA_DIR = os.path.join(BASE_DIR, "data")
TEMP_DIR = os.path.join(DATA_DIR, "temp")
LOG_DIR = os.path.join(BASE_DIR, "logs")
DB_DIR = os.path.join(DATA_DIR, "db")
COG_DIR = os.path.join(BASE_DIR, "cogs")

BOTNAME_LOG_DIR = os.path.join(LOG_DIR, BOTNAME)

REQUIRED_DIRS = [DATA_DIR, LOG_DIR, DB_DIR, COG_DIR, BOTNAME_LOG_DIR, TEMP_DIR]

for d in REQUIRED_DIRS:
    if os.path.isdir(d):
        continue
    try:
        os.makedirs(d)
    except FileExistsError:
        pass

sys.path.insert(1, os.path.join(COG_DIR))

config_file = os.path.join(DATA_DIR, "{}.yaml".format(BOTNAME))
if os.path.isfile(config_file):
    with open(config_file, "r") as f:
        cfg = yaml.load(f)
else:
    import zurok_setup
    zurok_setup.setup()

DB = TinyDB(os.path.join(DB_DIR, "{}_db.json".format(BOTNAME)))


class Zurok(commands.Bot):
    def __init__(self, *args, config, **kwargs):
        super().__init__(*args, **kwargs)
        self.logger = start_logger()

        self.base_dir = BASE_DIR
        self.data_dir = DATA_DIR
        self.temp_dir = TEMP_DIR
        self.log_dir = BOTNAME_LOG_DIR
        self.db_dir = DB_DIR
        self.cog_dir = COG_DIR

        self.rp_data = None

        self.botname = BOTNAME
        self.cfg = config

        self.db = DB
        # if cfg["devbuild"]:
        #     self.db.purge_tables()
        self.users_table = self.db.table("users")
        self.guilds_table = self.db.table("guilds")
        self.emojis_table = self.db.table("emojis")
        self.roles_table = self.db.table("roles")

        self.stats_table = self.db.table("stats")

        self.login_time = None

        self.owner = None

    async def on_command_error(self, ctx, error):
        """The event triggered when an error is raised while invoking a command.
        ctx   : Context
        error : Exception
        """

        if ctx.command is None:
            return

        if hasattr(ctx.command, 'on_error'):
            return

        error = getattr(error, 'original', error)

        if isinstance(error, errors.NoPrivateMessage):
            await ctx.message.author.send("This command cannot be used in private messages.")

        elif isinstance(error, errors.DisabledCommand):
            await ctx.send("Sorry. This command is disabled and cannot be used.")

        elif isinstance(error, errors.CommandOnCooldown):
            await ctx.send("This command is on cooldown, please wait to use it again")

        elif isinstance(error, errors.BadArgument):
            await self.say_error(ctx, error)

        elif isinstance(error, errors.MissingRequiredArgument):
            await self.say_error(ctx, error)

        elif isinstance(error, errors.BotMissingPermissions):
            await ctx.send("The bot does not have the required permissions for this command")

        elif isinstance(error, errors.CommandNotFound):
            pass

        elif isinstance(error, errors.MissingPermissions):
            await ctx.send("You do not have the required permissions for this command")

        elif isinstance(error, errors.NotOwner):
            pass

        elif isinstance(error, errors.TooManyArguments):
            await self.report_error(ctx, error)
            # await self.say_error(ctx, error)

        elif isinstance(error, errors.UserInputError):
            await self.report_error(ctx, error)
            await self.say_error(ctx, error)

        elif isinstance(error, errors.DiscordException):
            await self.report_error(ctx, error)
            await self.say_error(ctx, error)

        elif ctx.command is None:
            pass
        else:
            await self.report_error(ctx, error)

            await self.say_error(ctx, error)

    async def log_error(self, ctx, error):
        self.logger.error("In {0.command.name}:".format(ctx))
        traceback.print_tb(error.__traceback__)
        self.logger.error("{0.__class__.__name__}: {0}".format(error))

    async def report_error(self, ctx, error):
        self.logger.error("In {0.command.name}:".format(ctx))
        traceback.print_tb(error.__traceback__)
        self.logger.error("{0.__class__.__name__}: {0}".format(error))

        if ctx.guild is None:
            ctx.guild = ctx.author

        await self.owner.send("Command: `{0.message.content}`\n\n"
                                "Guild: {0.guild.name} *({0.guild.id})*\n"
                                "Member: {0.author.name}#{0.author.discriminator}/{0.author.display_name} "
                                "*({0.author.id})*:\n".format(ctx) +
                                "```{0.__class__.__name__}: {0}```\n".format(error))

    async def say_error(self, ctx, error):
        await ctx.send("```fix\n{0.__class__.__name__}: {0}\n```".format(error))

    async def on_ready(self):
        self.logger.info('------')
        self.logger.info('Logged in as')
        self.logger.info(str(self.user.name))
        self.logger.info(str(self.user.id))
        self.logger.info('------')
        self.logger.info("In {} guilds".format(len(self.guilds)))
        self.logger.info('------')

        self.update_db_users()
        # self.update_db_guilds()

        self.login_time = datetime.now()

        # try:
        #     del self.cfg["api"]
        # except KeyError:
        #     pass

        try:
            del self.cfg["token"]
        except KeyError:
            pass

        self.owner = self.get_user(cfg["owner"])

        info_channel = self.get_channel(cfg["info_ch"])
        if info_channel is not None:
            if cfg["devbuild"]:
                welcome = self.user.name + " v2 (dev) online"
            else:
                welcome = self.user.name + " v2 online"
            await info_channel.send(welcome, delete_after=60)

    async def on_message(self, message):
        if not message.author.bot:
            if self.user.mentioned_in(message) and len(message.mentions) == 1 and len(message.content.strip()) <= 22:
                ctx = await self.get_context(message)
                ctx.command = self.all_commands["help"]
                ctx.message.content = self.cfg["prefix"] + "help"
                ctx.invoked_with = "help"
                ctx.prefix = cfg["prefix"]

            if message.content.startswith(self.command_prefix):
                cmd = message.content.split(" ")[0].strip(self.command_prefix)
                self.logger.info("{}/{} {}".format(message.author.name, message.author.display_name, message.content))

                if message.guild is None:
                    source = "PM-" + str(message.author.id)
                else:
                    source = str(message.guild.id)

                if not self.stats_table.contains(Query().name == cmd):
                    self.stats_table.insert({"name": cmd, source: 1, "total": 1})
                else:
                    cmd_stats = self.stats_table.get(Query().name == cmd)

                    if source not in cmd_stats:
                        cmd_stats[source] = 0

                    cmd_stats[source] += 1
                    cmd_stats["total"] += 1

                    self.stats_table.update(cmd_stats, Query().name == cmd)

                try:
                    await self.process_commands(message)
                except Exception as err:
                    self.logger.error("BOT - " + str(err))
                    # await message.channel.send("ERROR - " + str(err))
        if message.author.id == 366322747401306122:
            self.logger.info("Github update detected. Restarting")
            if cfg["devbuild"]:
                self.logger.info("DEV BUILD - WILL NOT RESTART")
            await self.update_bot()
            await self.restart_bot()

    async def on_voice_state_update(self, member, before, after):
        if before.deaf != after.deaf:
            if after.deaf:
                self.logger.info("{0.name} ({0.id}): deafened by guild".format(member))
            else:
                self.logger.info("{0.name} ({0.id}): un-deafened by guild".format(member))

        if before.mute != after.mute:
            if after.mute:
                self.logger.info("{0.name} ({0.id}): muted by guild".format(member, after.channel))
            else:
                self.logger.info("{0.name} ({0.id}): un-muted by guild".format(member, after.channel))

        if before.self_deaf != after.self_deaf:
            if after.self_deaf:
                self.logger.info("{0.name} ({0.id}): deafened themselves".format(member))
            else:
                self.logger.info("{0.name} ({0.id}): un-deafened themselves".format(member))

        if before.self_mute != after.self_mute:
            if after.self_mute:
                self.logger.info("{0.name} ({0.id}): muted themselves".format(member))
            else:
                self.logger.info("{0.name} ({0.id}): un-muted themselves".format(member))

        if before.channel != after.channel:
            if before.channel is None:
                self.logger.info("{0.name} ({0.id}): joined voice channel {1.name} ({1.id})"
                                 .format(member, after.channel))
            elif after.channel is None:
                self.logger.info("{0.name} ({0.id}): left voice channel {1.name} ({1.id})"
                                 .format(member, before.channel))
            else:
                self.logger.info("{0.name} ({0.id}): switched voice channel {1.name} ({1.id}) -> {2.name} ({2.id})"
                                 .format(member, before.channel, after.channel))

    async def on_raw_reaction_add(self, emoji, message_id, channel_id, user_id):
        ch = bot.get_channel(channel_id)
        msg = await ch.get_message(message_id)
        user = msg.guild.get_member(user_id)

        if not user.bot:
            if user.id != cfg["owner"]:
                await msg.remove_reaction(emoji, user)
                return

            if msg.author.id != bot.user.id:
                return

            if emoji.name == "\u2734":
                bot.logger.info("User {}, deleted bot message id:{} ch:{} g:{} - {}".format(
                    user.name, msg.id, ch.id, msg.guild.id, msg.content))
                await msg.delete()

    def update_db_users(self):
        User = Query()
        for m in self.get_all_members():
            if m.nick is None:
                nick = []
            else:
                nick = [m.nick]

            roles = [r.id for r in m.roles if r.name != "@everyone"]

            if self.users_table.contains(User.id == m.id):
                m2 = self.users_table.get(User.id == m.id)

                m2["nicknames"].extend(nick)
                m2["nicknames"] = list(set(m2["nicknames"]))

                m2["guilds"].append(m.guild.id)
                m2["guilds"] = list(set(m2["guilds"]))

                self.users_table.update(m2, User.id == m.id)
            else:
                self.users_table.insert({"name": m.name, "id": m.id, "nicknames": nick, "guilds": [m.guild.id],
                                         "roles": roles})

    def update_db_guilds(self):
        for g in self.guilds:
            if not self.guilds_table.contains(Query().id == g.id):
                self.guilds_table.insert({"name": g.name, "id": g.id, "owner": {"id": g.owner.id, "name": g.owner.name},
                                          "region": str(g.region)})

            self.emojis_table.insert_multiple([{"id": e.id, "name": e.name, "guild": e.guild.id} for e in g.emojis
                                               if not self.emojis_table.contains(Query().id == e.id)])

            self.roles_table.insert_multiple([{"id": r.id, "name": r.name, "guild": r.guild.id} for r in g.roles
                                              if not self.emojis_table.contains(Query().id == r.id)])

    async def update_bot(self):
        if not self.cfg["devbuild"]:
            update_str = ""

            try:
                if platform in ["linux", "linux2", "darwin"]:
                    update_str = subprocess.check_output("git pull origin master", shell=True)
                elif platform == "win32":
                    update_str = subprocess.check_output("git pull origin master")

                update_str = update_str.decode("utf-8")
            except Exception as err:
                self.logger.error("Updater: " + str(err))
                update_str = str(err)

            return update_str

        return "Dev build, unable to update"

    async def restart_bot(self):
        if not self.cfg["devbuild"]:
            self.logger.info("##########")
            self.logger.info("")
            self.logger.info("RESTARTING")
            self.logger.info("")
            self.logger.info("##########")

            os.execv(sys.executable, [sys.executable, __file__, BOTNAME])

    async def get_member(self, member_id):
        for guild in self.guilds:
            try:
                return guild.get_member(member_id)
            except:
                pass


class Checks:
    @staticmethod
    def restricted_cmd():
        async def predicate(ctx):
            if ctx.message.author.id in cfg["roles"]["moderators"] or ctx.message.author.id in cfg["roles"]["admins"] \
                    or ctx.message.author.id == cfg["owner"]:
                return True
            else:
                return False

        return commands.check(predicate)

    @staticmethod
    def super_cmd():
        async def predicate(ctx):
            if ctx.message.author.id in cfg["roles"]["admins"] or ctx.message.author.id == cfg["owner"]:
                return True
            else:
                return False

        return commands.check(predicate)

    @staticmethod
    def disabled_cmd():
        async def predicate(ctx):
            return False

        return commands.check(predicate)

    @staticmethod
    def testing_cmd():
        async def predicate(ctx):
            if ctx.message.author.id in cfg["roles"]["moderators"] or ctx.message.author.id in cfg["roles"]["admins"] \
                    or ctx.message.author.id == cfg["owner"] or ctx.message.author.id in cfg["roles"]["testers"]:
                return True
            else:
                return False

        return commands.check(predicate)


class MainCog:
    def __init__(self, _bot):
        self.bot = _bot

    @commands.command()
    async def age(self, ctx):
        """Displays the age of the bot account"""
        await ctx.send(str(datetime.now() - self.bot.user.created_at))

    @commands.command()
    async def uptime(self, ctx):
        """View how long the bot has been online for"""
        await ctx.send(str(datetime.now() - self.bot.login_time))

    @Checks.restricted_cmd()
    @commands.command()
    async def update(self, ctx):
        """Updates bot to latest version"""
        await ctx.send("Updating...")
        self.bot.logger.info("Updating...")

        update_str = await self.bot.update_bot()
        if update_str != "" and update_str is not None:
            await ctx.send("```" + str(update_str) + "```")

        await self.bot.restart_bot()

    @Checks.restricted_cmd()
    @commands.command()
    async def restart(self, ctx):
        """Restarts the bot"""
        await ctx.send("Restarting...")
        await self.bot.restart_bot()

    @Checks.restricted_cmd()
    @commands.command()
    async def bork(self, ctx):
        """Deliberately broken code to test error reporting"""
        await ctx.send("This command is designed to fail. It is not a bug, it is a feature")
        x = str(int("abc"))
        self.bot.logger.warning("The <bork> command was designed to throw an error, it is not a bug it is a feature")

    @commands.command()
    async def info(self, ctx):
        """Bot information"""
        await ctx.send(
            "Zur'ok\n"
            "A random bot for a couple of private servers\n"
            "made with: `discord.py (rewrite)`  &  `python (3.6.4)`\n"
            "prefix: `{}`\n".format(self.bot.cfg["prefix"])
        )


def start_logger():
    log_file = datetime.now().strftime(BOTNAME_LOG_DIR + "/%H-%M_%d-%m-%Y.log")
    formatter = logging.Formatter("%(asctime)s:%(levelname)s:%(name)s: %(message)s")

    logger = logging.getLogger("discord")
    logger.propagate = False
    logger.setLevel(logging.DEBUG)

    handler = logging.FileHandler(filename=log_file, encoding="utf-8", mode="w")
    handler.setFormatter(formatter)

    ch = logging.StreamHandler()
    ch.setLevel(logging.INFO)
    ch.setFormatter(formatter)

    logger.handlers = [handler, ch]
    return logger


if __name__ == "__main__":
    bot = Zurok(command_prefix=cfg["prefix"], description=cfg["description"], config=cfg)

    bot.add_cog(MainCog(bot))
    bot.load_extension("cog_manager")

    bot.run(cfg["token"])

    bot.loop.run_until_complete(bot.logout())
