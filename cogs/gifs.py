import json
import math
import os
import random
from copy import copy

from discord.ext import commands
from natsort import natsort
from tabulate import tabulate
from tinydb import Query

from zurok import Checks

gif_list_file = os.getcwd() + "/gifs.json"


class Gifs:
    def __init__(self, bot):
        self.bot = bot

        self.gif_table = self.bot.db.table("gifs")
        self.gif_template = {"name": "", "hidden": False, "url": ""}

        if self.bot.cfg["devbuild"] and len(self.gif_table.all()) <= 0:
            with open(gif_list_file, "r") as f:
                self.gif_table.insert_multiple(json.load(f))

    @commands.group(aliases=["gifs"])
    async def gif(self, ctx, name: str=None):
        """Show a gif <name> or a random gif"""
        if name is None:
            r_gif = random.choice(self.gif_table.all())
            if r_gif["hidden"]:
                await ctx.send("You've found a hidden gif! `{}`\n{}".format(r_gif["name"], r_gif["url"]))
            else:
                await ctx.send("`{}`\n{}".format(r_gif["name"], r_gif["url"]))
        else:
            if self.gif_table.contains(Query().name == name):
                await ctx.send(self.gif_table.get(Query().name == name)["url"])
            else:
                await ctx.send("No gif found with that name.\nTry {prefix}giffer search".format(
                               prefix=self.bot.cfg["prefix"]))

    @commands.group()
    async def giffer(self, ctx):
        """Commands to manage gifs (add, remove, list)"""
        if ctx.invoked_subcommand is None:
            await ctx.send("Invalid sub-command!\n Use `{prefix}help giffer` to view sub-commands".format(
                prefix=self.bot.cfg["prefix"]))

    @giffer.command()
    async def add(self, ctx, name: str, url: str, hidden=False):
        """Add a gif"""
        if not url.startswith("http://") and not url.startswith("https://"):
            await ctx.send("`{}` is not a valid url!\n`{}`giffer add <name> <url>".format(url, self.bot.cfg["prefix"]))
            return

        if self.gif_table.contains(Query().name == name):
            await ctx.send("Gif exists with that name")
        else:
            if name.startswith("-"):
                await ctx.send("Gif name cannot start with `-`")
            else:
                self.gif_table.insert({"name": name, "hidden": hidden, "url": url})
                await ctx.send("Added gif: " + name)

    @giffer.command()
    @Checks.restricted_cmd()
    async def remove(self, ctx, name: str):
        """Remove a gif"""
        if not self.gif_table.contains(Query().name == name):
            await ctx.send("No gif exists with that name")
        else:
            self.gif_table.remove(Query().name == name)
            await ctx.send("Removed gif: " + name)

    @giffer.command()
    @Checks.restricted_cmd()
    async def hide(self, ctx, name: str):
        """Hide a gif"""
        if not self.gif_table.contains(Query().name == name):
            await ctx.send("No gif exists with that name")
        else:
            _gif = self.gif_table.get(Query().name == name)
            _gif["hidden"] = True
            self.gif_table.update(_gif, Query().name == name)
            await ctx.send("Hid gif: " + name)

    @giffer.command()
    @Checks.restricted_cmd()
    async def unhide(self, ctx, name: str):
        """Unhide a gif"""
        if not self.gif_table.contains(Query().name == name):
            await ctx.send("No gif exists with that name")
        else:
            _gif = self.gif_table.get(Query().name == name)
            _gif["hidden"] = False
            self.gif_table.update(_gif, Query().name == name)
            await ctx.send("Revealed gif: " + name)

    @giffer.command()
    async def list(self, ctx):
        """Display a list of gifs"""
        _gif_list = natsort.natsorted(self.gif_table.search(Query().hidden == False), key=lambda k: k["name"])

        if len(_gif_list) <= 0:
            await ctx.send("No gifs found!\nUse `{prefix}giffer add <name> <url>` to add a gif".format(
                prefix=self.bot.cfg["prefix"]))
        else:
            g = [x["name"] for x in _gif_list]

            await ctx.send("**" + str(len(g)) +
                           "** ```" + tabulate(self.make_tabulated_list(g), tablefmt="plain") + "```")

    @giffer.command(name="hidden")
    @Checks.restricted_cmd()
    async def list_hidden(self, ctx):
        """Display a list of hidden gifs"""
        _gif_list = sorted(self.gif_table.search(Query()["hidden"] == True), key=lambda k: k["name"])
        if len(_gif_list) <= 0:
            await ctx.send("No hidden gifs found!")
        else:
            g = [x["name"] for x in _gif_list]

            await ctx.send("**" + str(len(g)) +
                           "** ```" + tabulate(self.make_tabulated_list(g), tablefmt="plain") + "```")

    @giffer.command(name="rename")
    @Checks.restricted_cmd()
    async def rename_gif(self, ctx, old_name, new_name):
        """Rename a gif"""
        if not self.gif_table.contains(Query().name == old_name):
            await ctx.send("No gif exists with that name")
        else:
            _gif = self.gif_table.get(Query().name == old_name)
            _gif["name"] = new_name
            self.gif_table.update(_gif, Query().name == old_name)
            await ctx.send("Renamed gif: " + old_name + " to " + new_name)

    @giffer.command()
    async def search(self, ctx, name):
        """Search for a gif"""
        _gif_list = self.gif_table.search(Query().name.search(name))
        if len(_gif_list) <= 0:
            await ctx.send("No gifs found with search query: {}".format(name))
        else:
            g = [x["name"] for x in _gif_list if not x["hidden"]]
            await ctx.send("**" + str(len(g)) +
                           "** ```" + tabulate(self.make_tabulated_list(g), tablefmt="plain") + "```")

    def make_tabulated_list(self, gifs):
        cols = 6
        per_col = int(math.ceil(len(gifs) / cols))

        e = []
        g2 = [copy(e) for x in range(per_col)]

        for a in range(cols):
            for b in range(per_col):
                if len(gifs) == 0:
                    break
                gif = gifs.pop(0)
                g2[b].append(gif)

        return g2


def setup(bot):
    bot.add_cog(Gifs(bot))
