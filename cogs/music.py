import asyncio
import os
from copy import deepcopy

import discord
import praw
import youtube_dl
from discord.ext import commands
from tabulate import tabulate
from tinydb import Query

from zurok import Checks

# Suppress noise about console usage from errors
youtube_dl.utils.bug_reports_message = lambda: ''

ytdl_format_options = {
    'format': 'bestaudio/best',
    'outtmpl': 'data/temp/%(extractor)s-%(id)s-%(title)s.%(ext)s',
    'restrictfilenames': True,
    'noplaylist': True,
    'nocheckcertificate': True,
    'ignoreerrors': False,
    'logtostderr': False,
    'quiet': True,
    'no_warnings': True,
    'default_search': 'auto',
    'source_address': '0.0.0.0'  # ipv6 addresses cause issues sometimes
}

ffmpeg_options = {
    'before_options': '-nostdin',
    'options': '-vn'
}

ytdl = youtube_dl.YoutubeDL(ytdl_format_options)


class YTDLSource(discord.PCMVolumeTransformer):
    def __init__(self, source, *, added_by, data, volume=0.2):
        super().__init__(source, volume)

        self.data = data

        self.title = data.get('title')

        self.url = data.get('url')
        self.web_url = data.get("webpage_url")

        self.duration = data.get('duration')
        self.duration_str = str(int(self.duration/60)) + ":" + str(int(self.duration % 60)).rjust(2, "0")

        self.added_by = added_by

    @classmethod
    async def from_url(cls, url, user, *, loop=None):
        loop = loop or asyncio.get_event_loop()
        data = await loop.run_in_executor(None, ytdl.extract_info, url)

        if 'entries' in data:
            # take first item from a playlist
            data = data['entries'][0]

        filename = ytdl.prepare_filename(data)
        return cls(discord.FFmpegPCMAudio(filename, **ffmpeg_options), data=data, added_by=user)


class Music:
    def __init__(self, bot):
        self.bot = bot

        self.bot.logger.info("Loading opus....")
        if not discord.opus.is_loaded():
            discord.opus.load_opus('opus')
        self.bot.logger.info("Loaded opus!")

        self.default_volume = 0.2
        self.volume = {}

        self.song_queue = {}
        self.current_song = {}

        self.clean_voice_folder()

        self.skip_votes = {}
        self.votes_needed = 3

        self.idle_task = {}
        self.idle_count_needed = 5
        self.idle_count = 0

        self.ltt = praw.Reddit(client_id=self.bot.cfg["api"]["reddit"]["id"],
                               client_secret=self.bot.cfg["api"]["reddit"]["secret"],
                               user_agent=self.bot.cfg["api"]["reddit"]["agent"]).subreddit("listentothis")

        self.music_saved = self.bot.db.table("music_saved")

        self.music_saved_user = {"id": 0, "saved": []}

    async def on_ready(self):
        for guild in self.bot.guilds:
            if str(guild.id) not in self.song_queue:
                self.song_queue[str(guild.id)] = []

            if str(guild.id) not in self.skip_votes:
                self.skip_votes[str(guild.id)] = []

            if str(guild.id) not in self.skip_votes:
                self.current_song[str(guild.id)] = None

            if str(guild.id) not in self.volume:
                self.volume[str(guild.id)] = self.default_volume

    @commands.command()
    async def join(self, ctx):
        """Joins a voice channel"""
        if ctx.message.author.voice is not None:
            channel = ctx.message.author.voice.channel

            if ctx.voice_client is not None:
                if channel.id != ctx.voice_client.channel.id:
                    await ctx.voice_client.move_to(channel)
                else:
                    await ctx.send("Already in your voice channel")
                    return

            else:
                await channel.connect()

            if str(ctx.guild.id) not in self.idle_task:
                self.idle_task[str(ctx.guild.id)] = None
            self.idle_task[str(ctx.guild.id)] = self.bot.loop.create_task(self.idle_timer(ctx.guild.id))

    @commands.command()
    async def yt(self, ctx, *, url):
        """Streams audio from a youtube url"""
        self.stop_idle_task(ctx.guild.id)

        if ctx.voice_client is None:
            if ctx.author.voice.channel:
                await ctx.author.voice.channel.connect()

                if str(ctx.guild.id) not in self.idle_task:
                    self.idle_task[str(ctx.guild.id)] = None
            else:
                await ctx.send("Not connected to a voice channel. "
                               "(If you are in one, I'll join when you use this command)")
                return

        try:
            player = await YTDLSource.from_url(url, ctx.author.id, loop=self.bot.loop)
        except youtube_dl.utils.DownloadError as e:
            if "No video results" in str(e):
                msg = "No results for search term: `{}`".format(url)
            elif "This video is unavailable" in str(e):
                msg = "That video is unavailable or does not exist"
            elif "Incomplete YouTube ID" in str(e):
                msg = "The URL looks to be incomplete\nMake sure you have typed it/copied and pasted it correctly"
            elif "blocked it in your country" in str(e):
                msg = "That video has been blocked in my country, I am unable to play it"
            else:
                msg = "Unsupported error: " + str(e) + "\nPlease report this with `?bugs report <a brief description>`"

            await ctx.send(msg)
            return
        else:
            player.volume = self.volume[str(ctx.guild.id)]

        if ctx.voice_client.is_playing():
            if str(ctx.guild.id) not in self.song_queue:
                self.song_queue[str(ctx.guild.id)] = []
            self.song_queue[str(ctx.guild.id)].append(player)

            await ctx.send("*Queued:* {0.title} - *{0.duration_str}*".format(player))
            self.bot.logger.info("Queued: {0.title} - *{0.duration_str}*".format(player))

        else:
            ctx.voice_client.play(player, after=self.after_song)

            self.current_song[str(ctx.guild.id)] = player

            await ctx.send("*Now playing:* {0.title} - *{0.duration_str}*".format(player))
            self.bot.logger.info("Now playing: {0.title} - *{0.duration_str}*".format(player))

    @commands.command()
    async def listentothis(self, ctx, amount=5):
        """Gets songs from /r/listentothis"""
        self.stop_idle_task(ctx.guild.id)
        await ctx.send("Fetching {} songs from /r/listentothis".format(amount))

        amount = min(max(amount, 1), 25)
        songs = [x.url for x in self.ltt.hot(limit=amount*3) if ("youtube.com" in x.url or "youtu.be" in x.url)]

        if ctx.voice_client is None:
            if ctx.author.voice.channel:
                await ctx.author.voice.channel.connect()

                if str(ctx.guild.id) not in self.idle_task:
                    self.idle_task[str(ctx.guild.id)] = None
            else:
                await ctx.send("Not connected to a voice channel. "
                               "(If you are in one, I'll join when you use this command)")
                return

        for x in range(amount):
            url = songs[x]

            try:
                player = await YTDLSource.from_url(url, ctx.author.id, loop=self.bot.loop)
            except Exception:
                continue
            else:
                player.volume = self.volume[str(ctx.guild.id)]

            if ctx.voice_client is None:
                break

            if ctx.voice_client.is_playing():
                if str(ctx.guild.id) not in self.song_queue:
                    self.song_queue[str(ctx.guild.id)] = []
                self.song_queue[str(ctx.guild.id)].append(player)

                self.bot.logger.info("Queued: {0.title} - *{0.duration_str}*".format(player))

            else:
                ctx.voice_client.play(player, after=self.after_song)

                self.current_song[str(ctx.guild.id)] = player

                await ctx.send("*Now playing:* {0.title} - *{0.duration_str}*".format(player))
                self.bot.logger.info("Now playing: {0.title} - *{0.duration_str}*".format(player))

    @commands.command(name="volume")
    async def vol(self, ctx, volume=None):
        """Changes the player's volume (0 - 100)%"""
        if volume is None:
            await ctx.send("Current volume is {}%".format(int(self.volume[str(ctx.guild.id)]*100)))
            return

        if not volume.isdigit():
            await ctx.send("Volume must be a number!")
            return

        volume = int(volume)
        volume = min(max(0, volume), 100)
        volume = float(volume / 100)
        self.volume[str(ctx.guild.id)] = volume

        if ctx.voice_client is not None:
            if ctx.voice_client.source is not None:
                ctx.voice_client.source.volume = volume

        await ctx.send("Changed volume to {}%".format(int(volume*100)))
        self.bot.logger.info("Changed volume to {}%".format(int(volume*100)))

    @commands.command(aliases=["leave"])
    async def stop(self, ctx):
        """Stops playing and disconnects the bot from voice"""
        self.stop_idle_task(ctx.guild.id)

        if ctx.voice_client is not None:
            if ctx.voice_client.is_playing():
                ctx.voice_client.stop()

            await ctx.voice_client.disconnect()
            self.bot.logger.info("Stopped playing")
        else:
            await ctx.send("I'm not in a voice channel\nIf I am use {0}join, then {0}stop".format(self.bot.cfg["prefix"]))

    @commands.command()
    async def queue(self, ctx):
        """Show queue"""
        if str(ctx.guild.id) not in self.song_queue:
            self.song_queue[str(ctx.guild.id)] = []

        headers = ["Song", "Length", "Added By"]

        _q = self.song_queue[str(ctx.guild.id)]
        if len(_q) > 0:
            table = []
            for x, s in enumerate(self.song_queue[str(ctx.guild.id)]):
                m = ctx.guild.get_member(s.added_by)
                table.append([s.title, s.duration_str, m.name + "/" + m.display_name])
            await ctx.send("```" + tabulate(table, headers=headers) + "```")
        else:
            await ctx.send("Queue is empty")

    @commands.group()
    async def playing(self, ctx):
        """Show current song"""
        if str(ctx.guild.id) not in self.song_queue:
            self.song_queue[str(ctx.guild.id)] = []

        if str(ctx.guild.id) not in self.skip_votes:
            self.skip_votes[str(ctx.guild.id)] = []

        if str(ctx.guild.id) not in self.current_song:
            self.current_song[str(ctx.guild.id)] = None

        if ctx.invoked_subcommand is None:
            if self.current_song[str(ctx.guild.id)] is not None:
                m = ctx.guild.get_member(self.current_song[str(ctx.guild.id)].added_by)
                await ctx.send("{0.title} - *{0.duration_str}* - added by: {1.name}".format(
                    self.current_song[str(ctx.guild.id)], m))
            else:
                await ctx.send("Not playing anything")

    @playing.command()
    async def link(self, ctx):
        """Sends you the link for the current song"""
        if self.current_song[str(ctx.guild.id)] is None:
            await ctx.send("No song playing to send!")
            return

        song = self.current_song[str(ctx.guild.id)]

        await ctx.author.send(song.web_url)
        await ctx.send("Sent you the link for the current song! \u266C")

    @playing.command()
    async def save(self, ctx):
        """Saves the URL of the current playing song to your saved list"""
        if self.current_song[str(ctx.guild.id)] is None:
            await ctx.send("No song playing to save!")
            return

        if not self.music_saved.contains(Query().id == ctx.author.id):
            self.add_user(ctx.author)

        user_data = self.music_saved.get(Query().id == ctx.author.id)

        song = self.current_song[str(ctx.guild.id)]
        if song.web_url not in user_data["saved"]:
            user_data["saved"].append(song.web_url)
            self.bot.logger.info("{0.name} {0.id} saved song url {1.title}".format(ctx.author, song))

        self.music_saved.update(user_data, Query().id == ctx.author.id)

        await ctx.send("Added url for *{0.title}* to your saved songs!".format(self.current_song[str(ctx.guild.id)]))

    @commands.command()
    async def skip(self, ctx):
        """Skips current song (Instantly if you added the song)"""

        if str(ctx.guild.id) not in self.song_queue:
            self.song_queue[str(ctx.guild.id)] = []

        if str(ctx.guild.id) not in self.skip_votes:
            self.skip_votes[str(ctx.guild.id)] = []

        # skips immediatley if added song
        if ctx.author.id == self.current_song[str(ctx.guild.id)].added_by:
            if ctx.voice_client.is_playing():
                ctx.voice_client.stop()

            await ctx.send("Skipping current song")
            self.skip_votes[str(ctx.guild.id)] = []

        # +1 vote if not added song
        elif len(self.skip_votes[str(ctx.guild.id)]) >= self.votes_needed-1:
            if ctx.voice_client.is_playing():
                ctx.voice_client.stop()

            await ctx.send("Got enough votes. Skipping current song")
            self.skip_votes[str(ctx.guild.id)] = []
        else:
            if ctx.message.author.id not in self.skip_votes[str(ctx.guild.id)]:
                self.skip_votes[str(ctx.guild.id)].append(ctx.message.author.id)
                await ctx.send("{} votes to skip, votes needed: {}".format(len(self.skip_votes), self.votes_needed))
            else:
                await ctx.send("You cant vote to skip more than once")

    @commands.command(hidden=True)
    @Checks.restricted_cmd()
    async def tempfolder(self, ctx):
        files = [f for f in os.listdir(os.path.join(os.getcwd(), "data", "temp"))]
        if len(files) <= 0:
            await ctx.send("Empty")
        else:
            await ctx.send(",  ".join([f for f in os.listdir(os.path.join(os.getcwd(), "data", "temp"))]))

    @commands.command(hidden=True)
    @Checks.restricted_cmd()
    async def cleantempfolder(self, ctx):
        self.clean_voice_folder()

    def after_song(self, error):
        if error:
            self.bot.logger.error(error)

        for vc in self.bot.voice_clients:
            if vc.is_connected() and not vc.is_playing():
                self.current_song[str(vc.guild.id)] = None

        self.clean_voice_folder()
        self.next_song()

    def clean_voice_folder(self):
        # cleanup any download files
        for f in [f for f in os.listdir(os.path.join(os.getcwd(), "data", "temp"))]:
            try:
                os.remove(os.path.join(os.getcwd(), "data", "temp", f))
            except PermissionError:
                pass

    def next_song(self):
        # play next song in queue
        for vc in self.bot.voice_clients:
            guild_id = vc.guild.id
            if len(self.song_queue[str(guild_id)]) > 0 and self.current_song[str(guild_id)] is None:
                next_song = self.song_queue[str(guild_id)].pop(0)
                self.current_song[str(guild_id)] = next_song
                vc.play(next_song, after=self.after_song)
                self.bot.logger.info("Playing next song: {}".format(next_song.title))
            else:
                self.idle_task[str(guild_id)] = self.bot.loop.create_task(self.idle_timer(guild_id))

    async def idle_timer(self, guild_id):
        await self.bot.wait_until_ready()
        count = 0
        while count < self.idle_count_needed:
            await asyncio.sleep(60)
            count += 1
            self.bot.logger.info("Voice idle for {}/{} minutes".format(count, self.idle_count_needed))

        self.bot.logger.info("Leaving voice")
        vc = [x for x in self.bot.voice_clients if x.guild.id == guild_id][0]
        vc.stop()
        await vc.disconnect()

        self.stop_idle_task(guild_id)

    def stop_idle_task(self, guild_id):
        if str(guild_id) not in self.idle_task:
            self.idle_task[str(guild_id)] = None

        if self.idle_task[str(guild_id)] is not None:
            self.idle_task[str(guild_id)].cancel()
            self.idle_task[str(guild_id)] = None

    def add_user(self, user):
        if self.music_saved.contains(Query().id == user.id):
            raise Exception("Music Saved Table: User {0.name} {0.id} already exists!!".format(user))

        blank = deepcopy(self.music_saved_user)
        blank["id"] = user.id

        self.music_saved.insert(blank)


def setup(bot):
    bot.add_cog(Music(bot))
