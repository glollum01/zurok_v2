from copy import deepcopy
from datetime import datetime, timedelta

import discord
from discord.ext import commands
from tinydb import Query

from zurok import Checks


class Points:
    def __init__(self, bot):
        self.bot = bot

        self.daily = 100

        self.repeat_penalty_list = [100, 60, 40, 20]
        self.repeat_penalty_table = {str(x): y for x, y in enumerate(self.repeat_penalty_list)}

        self.points_table = self.bot.db.table("points2")
        # self.points_table.purge()
        self.blank_points = {"id": 0, "points": 0, "history": [], "daily": self.daily, "sent": []}

        self.hist_obj = {
            "amount": 0, "date": "", "type": "",
            "user": {"id": 0, "name": "", "display name": ""},
            "time": {"h": 0, "m": 0, "s": 0},
            "channel": {"id": 0, "name": ""},
            "guild": {"id": 0, "name": ""}
        }

        self.last_refresh = datetime.now()
        self.refresh()

        self.bot.reward_user = self.reward_user

    @commands.group()
    @commands.guild_only()
    async def points(self, ctx):
        """Commands to manage points"""
        if ((self.last_refresh + timedelta(days=1)) - datetime.now()).total_seconds() < 0:
            self.refresh()
            self.bot.logger.info("Refreshing points allowance")
        if ctx.invoked_subcommand is None:
            await ctx.send("Invalid sub-command!\n Use `{prefix}help points` to view sub-commands".format(
                prefix=self.bot.cfg["prefix"]))

    @points.command()
    async def give(self, ctx, points: str="0", user=None):
        """?points give <amount> <user>"""
        if user is not None:
            uid = user.strip("<@!>")
        else:
            await ctx.send("Second argument needs to be an @mention!")
            return

        if uid.isdigit():
            uid = int(uid)
            user = ctx.guild.get_member(uid)
        if not (isinstance(user, discord.Member) or isinstance(user, discord.User)):
            await ctx.send("Second argument needs to be an @mention!")
            return

        if not self.points_table.contains(Query().id == user.id):
            self.add_user(user.id)
        if not self.points_table.contains(Query().id == ctx.author.id):
            self.add_user(ctx.author.id)

        if points.isdigit():
            points = int(points)
        elif points.startswith("-"):
            await ctx.send("Points can't be negative!")
            return
        else:
            await ctx.send("Points need to be numbers!")
            return

        if points == 0:
            await ctx.send("You need to give more than 0")
            return

        if ctx.author.id == user.id:
            await ctx.send("You cant give points to yourself")
            return

        sender = self.points_table.get(Query().id == ctx.author.id)
        recipient = self.points_table.get(Query().id == user.id)

        max_points = self.get_gifting_limit_by_dict(user, sender)

        if max_points < points:
            await ctx.send("You can give up to {} points to {} today. Points left: {}"
                           .format(max_points, user.display_name, sender["daily"]))
        elif sender["daily"] < points:
            await ctx.send("You don't have enough points to give\nYou can give up to {} points each day."
                           "Points left today: {}"
                           .format(self.daily, sender["daily"]))
        else:
            sender["daily"] -= points
            recipient["points"] += points

            sender["sent"].append(self.make_history_entry(user, points, ctx.channel, ctx.guild, "sent"))
            recipient["history"].append(self.make_history_entry(ctx.author, points, ctx.channel, ctx.guild, "received"))

            await ctx.send("You gave {0.display_name} **{1}** points!".format(user, points))

            self.bot.logger.info("Points: {0.name} {0.id} >> {2} >> {1.name} {1.id}".format(ctx.author, user, points))

        self.points_table.update(sender, Query().id == ctx.author.id)
        self.points_table.update(recipient, Query().id == user.id)

    @points.command(aliases=["leader", "leaderboard"])
    async def board(self, ctx):
        """Leaderboard"""
        users = self.points_table.all()
        users = sorted(users, key=lambda k: k["points"], reverse=True)[:10]

        members = []
        for user in users:
            member = ctx.guild.get_member(user["id"])
            if member is not None:
                user["m"] = member
                members.append(user)
        _ = []
        for x, user in enumerate(members):
            member = user["m"]
            _.append("[" + str(x+1) + "] " + member.display_name.ljust(30) + str(user["points"]).rjust(5))
        await ctx.send("```" + "\n".join(_) + "```")

    @points.command(aliases=["tx"])
    async def history(self, ctx, user=None):
        """View your own or another users points history"""
        if user is None:
            user = ctx.author
        else:
            uid = user.strip("<@!>")
            if uid.isdigit():
                uid = int(uid)
                user = ctx.guild.get_member(uid)
            if not (isinstance(user, discord.Member) or isinstance(user, discord.User)):
                await ctx.send("Second argument needs to be an @mention!")
                return

        u = self.points_table.get(Query().id == user.id)
        tx = u["history"]
        if len(tx) <= 0:
            await ctx.send("No points history for " + user.display_name)
            return
        _ = []
        for x, t in enumerate(reversed(tx)):
            j = "{} - {}/{} - {}pts".format(t["date"], t["user"]["name"], t["user"]["display name"], t["amount"])
            _.append("[" + str(x+1) + "] " + j)
        await ctx.send("Points history. Most recent first\n```" + "\n".join(_[:9]) + "```")

    @points.command()
    async def view(self, ctx, user=None):
        """View your own or another users balance"""
        if user is None:
            user = ctx.author
        else:
            uid = user.strip("<@!>")
            if uid.isdigit():
                uid = int(uid)
                user = ctx.guild.get_member(uid)
            if not (isinstance(user, discord.Member) or isinstance(user, discord.User)):
                await ctx.send("First argument needs to be an @mention!")
                return

        if not self.points_table.contains(Query().id == user.id):
            self.add_user(user.id)

        _user = self.points_table.get(Query().id == user.id)

        await ctx.send("{0.display_name} has **{1}** points".format(user, _user["points"]))

    @points.command()
    @Checks.super_cmd()
    async def reward(self, ctx, user=None, points: str="30"):
        """"""
        uid = user.strip("<@!>")
        if uid.isdigit():
            uid = int(uid)
            user = ctx.guild.get_member(uid)
        if not (isinstance(user, discord.Member) or isinstance(user, discord.User)):
            await ctx.send("First argument needs to be an @mention!")
            return

        if not self.points_table.contains(Query().id == user.id):
            self.add_user(user.id)
        if not self.points_table.contains(Query().id == ctx.author.id):
            self.add_user(ctx.author.id)

        if points.isdigit():
            points = int(points)
        elif points.startswith("-"):
            await ctx.send("Points can't be negative!")
        else:
            await ctx.send("Points need to be numbers!")
            return

        if ctx.author.id == user.id:
            await ctx.send("You cant give points to yourself")
            return

        if not self.points_table.contains(Query().id == user.id):
            self.add_user(user.id)

        recipient = self.points_table.get(Query().id == user.id)

        recipient["points"] += points

        recipient["history"].append(self.make_history_entry(ctx.author, points, ctx.channel, ctx.guild, "reward"))

        await ctx.send("Reward of **{1}** points given to {0.display_name}!".format(user, points))

        self.bot.logger.info("Points: Reward >> {1} >> {0.name} {0.id}".format(user, points))

        self.points_table.update(recipient, Query().id == user.id)

    @points.command(name="daily")
    async def _daily(self, ctx):
        """View how many daily giftable points you have left"""
        if not self.points_table.contains(Query().id == ctx.author.id):
            self.add_user(ctx.author.id)

        user = self.points_table.get(Query().id == ctx.author.id)

        await ctx.send("You have {} daily points left".format(user["daily"]))

    @points.group()
    @Checks.super_cmd()
    async def admin(self, ctx):
        """~"""
        if ctx.invoked_subcommand is None:
            await ctx.send("Invalid sub-command!\n Use `{prefix}help points admin` to view sub-commands".format(
                prefix=self.bot.cfg["prefix"]))

    @admin.command()
    async def history(self, ctx, user=None, sent_rcvd=None, limit="9"):
        """View your own or another users points history"""
        if user is None:
            user = ctx.author
        else:
            uid = user.strip("<@!>")
            if uid.isdigit():
                uid = int(uid)
                user = self.bot.get_user(uid)

        if limit.isdigit():
            limit = int(limit)
        else:
            limit = 9

        if sent_rcvd in ["sent"]:
            sent_history = True
        else:
            sent_rcvd = "received"
            sent_history = False

        if not (isinstance(user, discord.Member) or isinstance(user, discord.User)):
            await ctx.send("First argument needs to be an @mention!")
            return

        if not self.points_table.contains(Query().id == user.id):
            self.add_user(user.id)

        u = self.points_table.get(Query().id == user.id)

        if sent_history:
            tx = u["sent"]
        else:
            tx = u["history"]

        limit = max(1, min(limit, len(tx)))
        if len(tx) <= 0:
            await ctx.send("No points history for " + user.display_name)
            return
        _ = []
        for x, t in enumerate(reversed(tx)):
            j = "{} {}:{}:{} {}/{} c:{} g:{} p:{}".format(t["date"], t["time"]["h"], t["time"]["m"], t["time"]["s"],
                                                    t["user"]["name"], t["user"]["display name"],
                                                    t["channel"]["name"], t["guild"]["name"],
                                                    t["amount"])
            _.append("[" + str(x+1) + "] " + j)
        await ctx.send("Points {} history for {}. Most recent first\n```".format(sent_rcvd, user.name)
                       + "\n".join(_[:limit]) + "```")

    @admin.command()
    async def limit(self, ctx, sender=None, recipient=None):
        """View senders limit for sending to recipient"""
        if recipient is not None:
            uid = recipient.strip("<@!>")
        else:
            await ctx.send("Recipient needs to be an @mention!")
            return

        if uid.isdigit():
            uid = int(uid)
            recipient = self.bot.get_user(uid)
        if not (isinstance(recipient, discord.Member) or isinstance(recipient, discord.User)):
            await ctx.send("Recipient needs to be an @mention!")
            return

        if sender is not None:
            uid = sender.strip("<@!>")
        else:
            await ctx.send("Sender needs to be an @mention!")
            return

        if uid.isdigit():
            uid = int(uid)
            sender = self.bot.get_user(uid)
        if not (isinstance(sender, discord.Member) or isinstance(recipient, discord.User)):
            await ctx.send("Sender needs to be an @mention!")
            return

        sender_data = self.points_table.get(Query().id == sender.id)
        recipient_data = self.points_table.get(Query().id == recipient.id)

        max_points = self.get_gifting_limit_by_dict(recipient, sender_data)

        await ctx.send("{0.name}  >>  **{2}**  >>  {1.name}".format(sender, recipient, max_points))

    def refresh(self):
        self.last_refresh = datetime.now()
        self.points_table.update({"daily": self.daily})

    def add_user(self, _id):
        user = dict(self.blank_points)
        user["id"] = _id
        self.points_table.insert(user)

    def make_history_entry(self, user, points, channel, guild, type):
        entry = deepcopy(self.hist_obj)
        dt = datetime.now()
        time = dt.time()
        date = dt.date()

        entry["amount"] = points
        entry["type"] = type
        entry["date"] = str(date)

        entry["time"]["h"] = time.hour
        entry["time"]["m"] = time.minute
        entry["time"]["s"] = time.second

        entry["user"]["id"] = user.id
        entry["user"]["name"] = user.name
        entry["user"]["display name"] = user.display_name

        entry["channel"]["id"] = channel.id
        entry["channel"]["name"] = channel.name

        entry["guild"]["id"] = guild.id
        entry["guild"]["name"] = guild.name

        return entry

    def get_gifting_limit_by_dict(self, recipient, sender_db_data):
        points_today = 0

        today = datetime.now()
        days = 0

        for j in reversed(sender_db_data["sent"]):
            if j["user"]["id"] == recipient.id:
                print("id match")
                date = datetime.strptime(j["date"], "%Y-%m-%d")

                if date.date() == today.date():
                    points_today += j["amount"]

                diff = today - date

                if diff.days > 7:
                    break

                if diff.days > days:
                    days = diff.days

        limit = max(0, self.repeat_penalty_table.get(str(days), self.repeat_penalty_list[-1]) - points_today)

        return limit

    async def reward_user(self, user, points, channel, guild):
        if not self.points_table.contains(Query().id == user.id):
            self.add_user(user.id)

        recipient = self.points_table.get(Query().id == user.id)

        recipient["points"] += points

        recipient["history"].append(self.make_history_entry(self.bot.user, points, channel, guild, "reward"))

        self.points_table.update(recipient, Query().id == user.id)


def setup(bot):
    bot.add_cog(Points(bot))
