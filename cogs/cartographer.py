import asyncio
import os
import random
import uuid

import discord
from discord.ext import commands

from deserts import terrain


class Cartographer:
    def __init__(self, bot):
        self.bot = bot

        self.modes = ["shore", "island", "mountain", "desert"]

        self.queue = []
        self.q_checker = None

        self.DATA_DIR = os.path.join(self.bot.data_dir, __name__)

        try:
            os.mkdir(self.DATA_DIR)
        except:
            pass

        self.clean_data_folder()

    @commands.command()
    async def mapper(self, ctx, mode: str="shore, island, mountain, desert"):
        """Generates a map using mewo2's deserts"""
        if isinstance(mode, list) and not isinstance(mode, str):
            mode = random.choice(mode)
        if mode not in self.modes:
            mode = random.choice(self.modes)

        if len(self.queue) == 0:
            await ctx.send("Please wait. This may take a few minutes")

            name = str(uuid.uuid4())
            self.queue.append({"name": name, "channel": ctx.channel, "ctx": ctx})

            _map = self.bot.loop.run_in_executor(None, self.gen_map, name, mode)

            await self.start_checker_task(_map)
        else:
            await ctx.send("Currently generating a map, please be patient it can take a while.")

    def gen_map(self, name, mode):
        # modes = ["shore", "island", "mountain", "desert"]
        # mode = random.choice(modes)

        self.bot.logger.info("Starting map generation ({})".format(mode))

        terrain.plt.close('all')
        while True:
            try:
                m = terrain.MapGrid(mode=mode)
                filename = os.path.join(self.DATA_DIR, "{}.png".format(name))
                m.plot(filename)

                self.bot.logger.info("Finished map generation")
                return filename
            except AssertionError:
                self.bot.logger.info("Failed assertion, retrying")
                return False

    async def queue_checker(self):
        await self.bot.wait_until_ready()
        while len(self.queue) > 0:
            await asyncio.sleep(15)
            for x, f in enumerate(self.queue):
                filename = os.path.join(self.DATA_DIR, "{}.png".format(f["name"]))
                if os.path.isfile(filename):
                    self.bot.logger.info("Sending map file {}".format(f["name"]))
                    await f["channel"].send(file=discord.File(filename))
                    try:
                        os.remove(filename)
                    except PermissionError:
                        self.bot.logger.error("PermissionError when deleting map: " + f["name"])
                    self.queue.pop(x)
                else:
                    self.bot.logger.info("Map {} not done".format(f["name"]))

        await self.stop_checker_task()

    async def start_checker_task(self, _map):
        self.bot.logger.info("Starting map generation checker")
        if self.q_checker is None:
            self.q_checker = self.bot.loop.create_task(self.queue_checker())

    async def stop_checker_task(self):
        self.bot.logger.info("Stopping map generation checker")
        if self.q_checker is not None:
            try:
                self.q_checker.cancel()
            except Exception as e:
                self.bot.logger.info(str(e))
            self.q_checker = None

    def clean_data_folder(self):
        for f in [f for f in os.listdir(self.DATA_DIR)]:
            try:
                os.remove(os.path.join(self.DATA_DIR, f))
            except PermissionError:
                self.bot.logger.error("PermissionError when deleting " + f)


def setup(bot):
    bot.add_cog(Cartographer(bot))
