import wikipedia
from discord.ext import commands


class Misc:
    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    async def wiki(self, ctx, *term: str):
        """Search wikipedia"""
        if len(term) <= 0:
            await ctx.send("I can't search wikipedia for nothing!")
            return
        term = " ".join(term)
        search = wikipedia.search(term)
        self.bot.logger.info("Getting results from Wikipedia - " + term)
        if len(search) <= 0:
            self.bot.logger.info("No results from Wikipedia - " + term)
            await ctx.send("No results for `" + term + "`")
        elif len(search) == 1 or search[0].lower() == term.lower():
            try:
                page = wikipedia.page(search[0])
            except wikipedia.DisambiguationError as e:
                await ctx.send("Ambiguous search term `" + e.title + "` may refer to:\n" + "\n".
                               join(["`"+x+"`" for x in e.options][:6] + ["..."]) +
                               "\n View full list at https://en.wikipedia.org"
                               "/wiki/" + e.title)
            else:
                self.bot.logger.info("Showing summary from Wikipedia - " + page.title)
                await ctx.send(page.title + "```" + wikipedia.summary(search[0], sentences=2) + "..\n```" + page.url)
        else:
            self.bot.logger.info("Showing " + str(len(search)) + " results from Wikipedia")
            await ctx.send("\n" + "\n".join(["`"+x+"`" for x in search][:10]))


def setup(bot):
    bot.add_cog(Misc(bot))
