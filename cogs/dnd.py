import json
import os
import random

import aiohttp
import discord
from discord.ext import commands


class DnD:
    def __init__(self, bot):
        self.bot = bot

        self.data = bot.rp_data

        with open(os.path.join(bot.data_dir, "dnd", "deck_of_many_things.json")) as f:
            self.domt_json = json.load(f)

        self.character_gen_url = "https://yorimor.com/dnd/character2/gen"
        self.character_show_url = "https://yorimor.com/dnd/character2/show/{id}"

        self.races = ["any", "dragonborn", "dwarf", "elf", "gnome", "half-elf", "half-orc", "halfling", "human", "tiefling"]
        self.classes = ["any", "barbarian", "druid", "paladin", "warlock", "wizard", "sorcerer"]

    @commands.group()
    async def dnd(self, ctx):
        """D&D related commands"""
        if ctx.invoked_subcommand is None:
            await ctx.send("Invalid sub-command!\n Use `{prefix}help {cmd}` to view sub-commands".format(
                prefix=self.bot.cfg["prefix"], cmd=ctx.command.name
            ))

    @dnd.command()
    async def domt(self, ctx):
        """Deck of Many Things"""
        drawn = random.choice(self.domt_json)
        await ctx.send("**{name}**\n```{desc}```".format(**drawn))

    @dnd.group()
    async def stats(self, ctx):
        """Rolls 4d6 and drops the lowest (x6)
        `?help dnd stats` for more options"""
        if ctx.invoked_subcommand.name is "stats":
            dice = self.nd6(4)
            dice.sort()

            await ctx.send(", ".join([str(x) for x in dice]))

    @stats.command()
    async def standard(self, ctx):
        """Standard stats array"""
        await ctx.send("8, 10, 12, 13, 14, 15")

    @stats.command(name="3d6")
    async def _3d6(self, ctx):
        """Rolls 3d6 (x6)"""
        dice = self.nd6(3)
        dice.sort()

        await ctx.send(", ".join([str(x) for x in dice]))

    @stats.command(name="5d6")
    async def _5d6(self, ctx):
        """Rolls 5d6, drops lowest 2 (x6)"""
        dice = self.nd6(5)
        dice.sort()

        await ctx.send(", ".join([str(x) for x in dice]))

    @stats.command()
    async def colville(self, ctx):
        """Rolls 4d6, drops lowest, re-rolls if 2 scores are not above 14 (x6)"""
        dice = self.nd6(4)
        dice.sort()

        while dice[-1] < 15 or dice[-2] < 15:
            dice = self.nd6(4)
            dice.sort()

        await ctx.send(", ".join([str(x) for x in dice]))

    @stats.command()
    async def colville2(self, ctx):
        """Rolls 4d6, drops lowest, re-rolls if total modifier is below 2 (x6)"""
        dice = self.nd6(4)
        dice.sort()

        while sum([(x - 10) // 2 for x in dice]) < 2:
            dice = self.nd6(4)
            dice.sort()

        await ctx.send(", ".join([str(x) for x in dice]))

    def roll_dice(self, amount, sides):
        for x in range(amount):
            yield random.randint(1, sides)

    def nd6(self, n: int):
        dice = []
        n = max(3, n)
        for s in range(6):
            _ = [d for d in self.roll_dice(n, 6)]
            _.sort()
            _ = _[-3:]
            dice.append(sum(_))
        return dice

    @dnd.command()
    async def mitem(self, ctx):
        """Random magic item"""
        await ctx.send(random.choice(self.data.magic_items.all())["str"])

    @dnd.command()
    async def spell(self, ctx):
        """Random spell"""
        spell = random.choice(self.data.spells.all())

        embed = discord.Embed(title=spell["name"], description="{}".format("\n".join(spell["text"])))

        embed.add_field(name="Level", value=spell["level"], inline=True)
        embed.add_field(name="Casting Time", value=spell["time"], inline=True)
        embed.add_field(name="Duration", value=spell["duration"], inline=True)

        embed.add_field(name="Range", value=spell["range"], inline=True)
        embed.add_field(name="Ritual", value=spell["ritual"], inline=True)
        embed.add_field(name="Components", value=spell["components"], inline=True)

        embed.set_footer(text=spell["classes"])

        await ctx.send(embed=embed)

    @dnd.command()
    async def trinket(self, ctx):
        """Random trinket"""
        await ctx.send(random.choice(self.data.trinkets.all())["str"])

    @dnd.command(hidden=True)
    async def monster(self, ctx):
        """Random monster"""
        monster = random.choice(self.data.monsters.all())

        embed = discord.Embed(title=monster["name"], description="""Type: {type}
            *Size*:.........{size}
            *Speed*:........{speed}
            *Senses*:.......{senses}
            *Languages*:....{languages}
            *Alignment*:....{alignment}""".format(**monster).title(), color=0x00ff00)
        embed.add_field(name="CR", value=monster["cr"], inline=True)
        embed.add_field(name="HP", value=monster["hp"], inline=True)
        embed.add_field(name="AC", value=monster["ac"], inline=True)

        embed.set_footer(text=monster["source"])

        await ctx.send(embed=embed)

    @dnd.command()
    async def pc(self, ctx, level="any", race="any", _class="any"):
        """Quick command for my character generator webpage at yorimor.com/character2"""
        if level == "any":
            level = random.randint(1, 20)
        elif level.isdigit():
            level = int(level)
            level = max(min(level, 20), 1)
        else:
            await ctx.send("<level> must be a whole number or `any`! not `"
                           + str(level) + "`\n`{}dnd pc <level> <race> <class>`")
            return

        if race not in self.races:
            await ctx.send("`{}` is not a valid race!"
                           "\nValid races: `{}`"
                           "\n`{}dnd pc <level> <race> <class>`".format(race, "`  `".join(self.races), self.bot.cfg["prefix"]))
            return

        if _class not in self.classes:
            await ctx.send("`{}` is not a valid class!"
                           "\nValid classes: `{}`"
                           "\n`{}dnd pc <level> <race> <class>`".format(_class, "`  `".join(self.classes), self.bot.cfg["prefix"]))
            return

        params = {"level": level, "race": race, "class": _class}
        self.bot.logger.info("DND PC - Generation request")

        async with aiohttp.ClientSession() as session:
            async with session.get(self.character_gen_url, params=params) as r:
                if r.status == 200:
                    self.bot.logger.info("DND PC - Getting character - " + str(r.url) + "/json")
                    async with session.get(str(r.url) + "/json") as r2:
                        if r2.status == 200:
                            char = json.loads(await r2.text())
                            await ctx.send("**{name}**\nLevel {level}, {race_name}, {class_name}\n\n"
                                           "STR:**{str}**  DEX:**{dex}**  CON:**{con}**\n"
                                           "INT:**{int}**  WIS:**{wis}**  CHA:**{cha}**\n".format(**char)
                                           + self.character_show_url.format(**char))
                            self.bot.logger.info("DND PC - Done!")
                        else:
                            await ctx.send(
                                "Something went wrong when communicating with the generator - ERR" + str(r.status_code))
                            self.bot.logger.info(
                                "Something went wrong when communicating with the generator - ERR" + str(r.status_code))
                else:
                    await ctx.send(
                        "Something went wrong when communicating with the generator - ERR" + str(r.status_code))
                    self.bot.logger.info(
                        "Something went wrong when communicating with the generator - ERR" + str(r.status_code))


def setup(bot):
    bot.add_cog(DnD(bot))
