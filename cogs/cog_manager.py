from copy import deepcopy
from os import listdir
from os.path import isfile, join

from discord.ext import commands
from tinydb import Query

from zurok import Checks, COG_DIR

Cog = Query()


# noinspection PyTypeChecker
class CogManager:
    def __init__(self, bot):
        self.bot = bot

        self.cogs_table = self.bot.db.table("cogs")
        self.cog_cfg_template = {"name": "", "enabled": True, "cfg": {}}

        self.update_cogs_db()
        self.load_cogs()

    @commands.group()
    @Checks.restricted_cmd()
    async def cogs(self, ctx):
        """For configuring cogs"""
        if ctx.invoked_subcommand is None:
            await ctx.send("Invalid sub-command!\n Use `{prefix}help {cmd}` to view sub-commands".format(
                prefix=self.bot.cfg["prefix"], cmd=ctx.command.name
            ))

    @cogs.command(aliases=["load"])
    @Checks.restricted_cmd()
    async def enable(self, ctx, cog_name):
        if self.cogs_table.contains(Cog.name == cog_name):
            if not self.cogs_table.get(Cog.name == cog_name)["enabled"]:
                try:
                    self.bot.load_extension(cog_name)
                except Exception as e:
                    await ctx.send("Failed to load cog " + cog_name)
                    self.bot.logger.info("Failed to load cog " + cog_name)
                    self.bot.logger.error(str(e))
                else:
                    c = self.cogs_table.get(Cog.name == cog_name)
                    c["enabled"] = True
                    self.cogs_table.update(c, Cog.name == cog_name)

                    await ctx.send("Loaded cog " + cog_name)
                    self.bot.logger.info("Loaded cog " + cog_name)
            else:
                await ctx.send("Cog already enabled")
        else:
            await ctx.send("No cog with name: " + cog_name)

    @cogs.command(aliases=["unload"])
    @Checks.restricted_cmd()
    async def disable(self, ctx, cog_name):
        if self.cogs_table.contains(Cog.name == cog_name):
            try:
                self.bot.unload_extension(cog_name)
            except Exception as e:
                await ctx.send("Failed to unload cog " + cog_name)
                self.bot.logger.info("Failed to unload cog " + cog_name)
                self.bot.logger.error(str(e))
            else:
                self.bot.logger.info("Unloaded cog " + cog_name)

                c = self.cogs_table.get(Cog.name == cog_name)
                c["enabled"] = False
                self.cogs_table.update(c, Cog.name == cog_name)

                await ctx.send("Disabled cog " + cog_name)
        else:
            await ctx.send("No cog with name: " + cog_name)

    @cogs.command()
    @Checks.restricted_cmd()
    async def reload(self, ctx, cog_name):
        if self.cogs_table.contains(Cog.name == cog_name):
            if self.cogs_table.get(Cog.name == cog_name)["enabled"]:
                await ctx.send("Reloading cog: " + cog_name)
                try:
                    self.bot.unload_extension(cog_name)
                except Exception as e:
                    await ctx.send("Failed to unload cog")
                    self.bot.logger.info("Failed to unload cog " + cog_name)
                    self.bot.logger.error(str(e))
                else:
                    self.bot.logger.info("Unloaded cog " + cog_name)
                    try:
                        self.bot.load_extension(cog_name)
                    except Exception as e:
                        await ctx.send("Failed to reload cog")
                        self.bot.logger.info("Failed to reload cog " + cog_name)
                        self.bot.logger.error(str(e))
                    else:
                        await ctx.send("Reloading cog: " + cog_name)
                        self.bot.logger.info("Reloaded cog " + cog_name)
            else:
                await ctx.send(cog_name + " has been disabled. Unable to reload")
        else:
            await ctx.send("No cog with name: " + cog_name)

    @cogs.command()
    @Checks.restricted_cmd()
    async def list(self, ctx):
        """Lists all cogs"""
        msg = "Cogs: {total} \n Enabled ({total_e}):`{e}` \n Disabled ({total_d}):`{d}`"
        cogs_list = self.list_cogs()
        msg = msg.format(total=str(len(cogs_list["enabled"])+len(cogs_list["disabled"])),
                   total_e=str(len(cogs_list["enabled"])),
                   total_d=str(len(cogs_list["disabled"])),
                   e="`  `".join(cogs_list["enabled"]),
                   d="`  `".join(cogs_list["disabled"])
                   )
        await ctx.channel.send(msg)

    def update_cogs_db(self):
        for c in self.list_cog_files():
            if not self.cogs_table.contains(Cog.name == c):
                self.bot.logger.info("Adding new cog to DB: {}".format(c))
                cog = deepcopy(self.cog_cfg_template)
                cog["name"] = c
                self.cogs_table.insert(cog)

    def load_cogs(self):
        c_list = self.list_cog_files()
        for c in self.cogs_table.search(Cog.enabled == True):
            if c["name"] not in c_list:
                self.cogs_table.remove(Cog.name == c["name"])
                self.bot.logger.info("Removing missing cog from db: " + c["name"])
            else:
                self.bot.logger.info("Loading cog " + c["name"] + "...")
                try:
                    self.bot.load_extension(c["name"])
                except Exception as e:
                    self.bot.logger.info("Failed to load cog " + c["name"])
                    self.bot.logger.error(str(e))
                else:
                    # self.bot.logger.info("Loaded cog " + c["name"] + "!")
                    self.bot.logger.info("Done!")

    def list_cogs(self):
        enabled = [c["name"] for c in self.cogs_table.search(Cog.enabled == True)]
        if len(enabled) <= 0:
            enabled = ["None"]
        disabled = [c["name"] for c in self.cogs_table.search(Cog.enabled == False)]
        if len(disabled) <= 0:
            disabled = ["None"]
        return {"enabled": enabled, "disabled": disabled}

    def list_cog_files(self):
        cogs_list = [f[0:-3] for f in listdir(COG_DIR) if isfile(join(COG_DIR, f)) if f not in ["__init__.py", "cog_manager.py"]]
        return cogs_list


def setup(bot):
    bot.add_cog(CogManager(bot))
