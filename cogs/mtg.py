import os

import requests
from discord.ext import commands
from tinydb import TinyDB


class MTG:
    def __init__(self, bot):
        self.bot = bot
        self.db_dir = os.path.join(self.bot.data_dir, "mtg")
        if not os.path.isdir(self.db_dir):
            os.makedirs(self.db_dir)
        self.db = TinyDB(os.path.join(self.db_dir, "mtg_cache.json"))

        self.scryfall = ScryfallAPI(db=self.db)

    @commands.command()
    async def tutor(self, ctx, *term):
        """Search for a Magic: The Gathering card by name"""
        if len(term) <= 0:
            card = await self.scryfall.random()
            await ctx.send(card["name"] + "\n" + card["image_uris"]["large"])
        else:
            term = " ".join(term)
            data = await self.scryfall.search(term)
            if data["total_cards"] == 0:
                await ctx.send("No cards found")
            elif data["total_cards"] == 1:
                card = data["data"][0]
                await ctx.send(card["name"] + "\n" + card["image_uris"]["large"])
            else:
                await ctx.send(str(data["total_cards"]) + " results\n```"
                               + "\n".join([x["name"] for x in data["data"][:20]]) + "```")


class ScryfallAPI:
    def __init__(self, db=None):
        self.url = "https://api.scryfall.com"
        self.delay = 100  # delay between requests in ms
        self.last_request = None

        self.db = None
        self.cache = None
        if db is not None:
            self.db = db
            self.cache = self.db.table("cache")

        self.fuzzy_url = self.url + "/cards/named?format=json&fuzzy={query}"
        self.exact_url = self.url + "/cards/named?format=json&exact={query}"
        self.search_url = self.url + "/cards/search?format=json&q={query}"
        self.random_url = self.url + "/cards/random?format=json"

    async def search(self, card):
        r = requests.get(self.search_url.format(query=card))
        if r.ok:
            return r.json()
        else:
            r.raise_for_status()

    async def exact(self, card):
        r = requests.get(self.exact_url.format(query=card))
        if r.ok:
            return r.json()
        else:
            r.raise_for_status()

    async def fuzzy(self, card):
        r = requests.get(self.fuzzy_url.format(query=card))
        if r.ok:
            return r.json()
        else:
            r.raise_for_status()

    async def random(self):
        r = requests.get(self.random_url)
        if r.ok:
            return r.json()
        else:
            r.raise_for_status()


def setup(bot):
    bot.add_cog(MTG(bot))
