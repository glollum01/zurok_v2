import asyncio
import random
from copy import deepcopy

import discord
from discord import Embed
from discord.ext import commands
from tinydb import Query


class TicTacToe:
    def __init__(self, bot):
        self.bot = bot

        self.tictactoe_table = self.bot.db.table("tictactoe")

        self.nought = "O"
        self.cross = "X"

        self.win_conditions = [[0,1,2], [3,4,5], [6,7,8], [0,3,6], [1,4,7], [2,5,8], [0,4,8], [2,4,6]]

        self.emjois = {
            "top left": "\u2196",
            "top": "\u2B06",
            "top right": "\u2197",
            "left": "\u2B05",
            "middle": "\u23FA",
            "right": "\u27A1",
            "bottom left": "\u2199",
            "bottom": "\u2B07",
            "bottom right": "\u2198"
        }

        self.reaction_list = [x for x in self.emjois]

        self.reation_functions = {
            self.emjois["top left"]: {"func": self.make_move, "args": {"position": 0}},
            self.emjois["top"]: {"func": self.make_move, "args": {"position": 1}},
            self.emjois["top right"]: {"func": self.make_move, "args": {"position": 2}},
            self.emjois["left"]: {"func": self.make_move, "args": {"position": 3}},
            self.emjois["middle"]: {"func": self.make_move, "args": {"position": 4}},
            self.emjois["right"]: {"func": self.make_move, "args": {"position": 5}},
            self.emjois["bottom left"]: {"func": self.make_move, "args": {"position": 6}},
            self.emjois["bottom"]: {"func": self.make_move, "args": {"position": 7}},
            self.emjois["bottom right"]: {"func": self.make_move, "args": {"position": 8}}
        }

        self.blank_game_data = {
            "message": {
                "id": 0,
                "channel": 0,
                "guild": 0
            },
            "player": 0,
            "state": [
                None, None, None,
                None, None, None,
                None, None, None
            ],
            "playing": True,
            "winner": None,
            "player turn": True
        }

    @commands.command()
    async def tictactoe(self, ctx):
        """Play tic-tac-toe with the bot"""
        if not self.tictactoe_table.contains(Query()["player"] == ctx.author.id):
            await self.new_game(ctx)
        else:
            await self.continue_game(ctx)

    async def new_game(self, ctx):
        game = deepcopy(self.blank_game_data)
        game["player"] = ctx.author.id

        embed_data = await self.create_embed(game)
        embed = embed_data[0]
        reactions = embed_data[1]

        msg = await ctx.send(embed=embed)

        for r in reactions:
            await msg.add_reaction(r)

        game["message"]["id"] = msg.id
        game["message"]["channel"] = msg.channel.id
        game["message"]["guild"] = msg.guild.id

        self.tictactoe_table.insert(game)

    async def continue_game(self, ctx):
        game = self.tictactoe_table.get(Query()["player"] == ctx.author.id)
        try:
            msg = await self.bot.get_channel(game["message"]["channel"]).get_message(game["message"]["id"])
        except Exception as e:
            self.bot.logger.error(str(e))

        if not game["playing"]:
            await self.new_game(ctx)
            self.tictactoe_table.remove(Query()["message"]["id"] == game["message"]["id"])
            return

        try:
            await msg.delete()
        except Exception as e:
            self.bot.logger.error(str(e))

        embed_data = await self.create_embed(game)
        embed = embed_data[0]
        reactions = embed_data[1]

        msg = await ctx.send(embed=embed)

        for r in reactions:
            await msg.add_reaction(r)

        game["message"]["id"] = msg.id
        game["message"]["channel"] = msg.channel.id
        game["message"]["guild"] = msg.guild.id

        self.tictactoe_table.update(game, Query()["player"] == ctx.author.id)

    async def make_move(self, game, position, bot=False):
        tile = self.cross
        if bot:
            tile = self.nought

        if game["state"][position] is None:
            game["state"][position] = tile

        game["player turn"] = bot

        self.tictactoe_table.update(game, Query()["message"]["id"] == game["message"]["id"])
        await self.update_embed(game["message"]["id"])

    async def create_embed(self, game):
        embed = Embed(title="Tic-Tac-Toe vs. {}".format(self.bot.user.display_name))

        state = await self.render_state(game["state"])

        embed.description = state[0]
        embed.colour = discord.Colour.blue()

        player = await self.bot.get_member(game["player"])

        if game["player turn"]:
            footer = player.name
        else:
            footer = self.bot.user.name

        if game["winner"] is not None:
            if game["winner"] == "Draw":
                footer = "Draw!"
            elif game["winner"] == self.cross:
                footer = "Winner! " + player.name
            elif game["winner"] == self.nought:
                footer = "Winner! " + self.bot.user.name

        embed.set_footer(text=footer)

        reactions = state[1]

        return embed, reactions

    async def render_state(self, state_data):

        reactions = []

        state_data2 = deepcopy(state_data)

        for x, s in enumerate(state_data):
            if s is None:
                state_data2[x] = "-"
                reactions.append(self.emjois[self.reaction_list[x]])

        s_table = "{0[0]} | {0[1]} | {0[2]}\n---------\n" \
                  "{0[3]} | {0[4]} | {0[5]}\n---------\n" \
                  "{0[6]} | {0[7]} | {0[8]}".format(state_data2)

        return s_table, reactions

    async def update_embed(self, message_id, end=False):
        game = self.tictactoe_table.get(Query()["message"]["id"] == message_id)
        embed_data = await self.create_embed(game)
        embed = embed_data[0]
        reactions = embed_data[1]
        channel = self.bot.get_channel(game["message"]["channel"])
        msg = await channel.get_message(message_id)
        embed.colour = discord.Colour.blue()
        await msg.edit(embed=embed)
        await msg.clear_reactions()

        if not end:
            await self.win_check(game)

        if not game["playing"]:
            return

        if not game["player turn"]:
            with channel.typing():
                await asyncio.sleep(3)
            await self.ai(game)

            for r in reactions:
                await msg.add_reaction(r)

    async def ai(self, game):
        open_slots = [x for x, i in enumerate(game["state"]) if i is None]
        game["player turn"] = True
        await self.make_move(game, random.choice(open_slots), bot=True)
        if len(open_slots) == 1:
            await self.end_game(game, "Draw")

    async def win_check(self, game):
        state = game["state"]
        for condition in self.win_conditions:
            if state[condition[0]] == state[condition[1]] == state[condition[2]]:
                if state[condition[0]] is None:
                    return
                await self.end_game(game, state[condition[0]])
                return
        return

    async def end_game(self, game, winner):
        game["playing"] = False
        game["winner"] = winner
        self.tictactoe_table.update(game, Query()["message"]["id"] == game["message"]["id"])
        await self.update_embed(game["message"]["id"], end=True)
        self.tictactoe_table.remove(Query()["message"]["id"] == game["message"]["id"])

    async def on_raw_reaction_add(self, emoji, message_id, channel_id, user_id):

        msg = await self.bot.get_channel(channel_id).get_message(message_id)
        user = msg.guild.get_member(user_id)

        if not user.bot:
            if not self.tictactoe_table.contains(Query()["message"]["id"] == message_id):
                return

            game_data = self.tictactoe_table.get(Query()["message"]["id"] == message_id)

            if user.id != game_data["player"]:
                return

            if emoji.name in self.reation_functions:
                func = self.reation_functions[emoji.name]["func"]
                args = self.reation_functions[emoji.name]["args"]
                await func(game_data, **args)

            try:
                await msg.remove_reaction(emoji, user)
            except discord.errors.NotFound:
                pass


def setup(bot):
    bot.add_cog(TicTacToe(bot))
