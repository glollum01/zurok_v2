import random

from discord.ext import commands


class Dice:
    def __init__(self, bot):
        self.bot = bot

        self.stats = ["STR", "CON", "DEX", "INT", "WIS", "CHA"]

    @commands.command(aliases=["roll"])
    async def dice(self, ctx, _dice: str):
        """Rolls dice in NdN e.g. 4d6
        4d6h3 - to keep (h)ighest 3
        4d6l3 - to keep (l)owest 3"""
        hi = False
        keep = -1
        if "d" in _dice:
            amount, sides = _dice.split("d")
            if amount.isdigit():
                amount = int(amount)
                if amount <= 0:
                    await ctx.send("Can't roll {} dice!".format(amount))
                    return
            else:
                await ctx.send(amount + " is not a positive whole number")
                return

            if "h" in sides or "l" in sides or "k" in sides:
                if "h" in sides:
                    hi = True
                    sides, keep = sides.split("h")
                elif "k" in sides:
                    hi = True
                    sides, keep = sides.split("k")
                elif "l" in sides:
                    hi = False
                    sides, keep = sides.split("l")

                if keep.isdigit():
                    keep = int(keep)
                    if keep <= 0 or keep > amount:
                        await ctx.send("Can't keep {} dice!".format(sides))
                        return

            if sides.isdigit():
                sides = int(sides)
                if sides <= 0:
                    await ctx.send("Can't roll a dice with {} sides!".format(sides))
                    return
            else:
                await ctx.send(sides + " is not a number")
                return

            rolls = [x for x in self.roll_dice(amount, sides)]
            total = sum(rolls)

            if keep > -1:
                rolls = sorted(rolls, reverse=hi)
                keep_dice = rolls[:keep]
                discard_dice = rolls[keep:]
                total = sum(keep_dice)
                rolls = [str(x) for x in keep_dice]
                rolls.extend(["~~" + str(x) + "~~" for x in discard_dice])

            await ctx.send("Total: **{total}**\n{dice}".format(total=str(total), dice=", ".join([str(x) for x in rolls])))

        else:
            await ctx.send("Needs to be in NdN format!\ne.g. 4d6")

    def roll_dice(self, amount, sides):
        for x in range(amount):
            yield random.randint(1, sides)

    def n4d6(self):
        dice = []
        for s in range(6):
            _ = [d for d in self.roll_dice(4, 6)]
            _.sort()
            _.pop(0)
            dice.append(sum(_))
        return dice


def setup(bot):
    bot.add_cog(Dice(bot))
