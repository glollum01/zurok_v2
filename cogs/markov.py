import json
import os
import random

import discord
import markovify
from discord import Embed
from discord.ext import commands

from utils import MarkovGenerator


# nlp = spacy.load('en_core_web_sm', parser=False, entity=False)
#
#
# class POSifiedText(markovify.Text):
#     def word_split(self, sentence):
#         return ["::".join((word.orth_, word.pos_)) for word in nlp(sentence)]
#
#     def word_join(self, words):
#         sentence = " ".join(word.split("::")[0] for word in words)
#         return sentence


class Markov:
    def __init__(self, bot):
        self.bot = bot

        with open(os.path.join(bot.data_dir, "markov", "monster_names.json"), encoding="utf-8") as f:
            self.monster_names = json.load(f)
        self.monster_markov = MarkovGenerator(dictionary=self.monster_names, ban_list=[])

        with open(os.path.join(bot.data_dir, "markov", "spell_names.json"), encoding="utf-8") as f:
            self.spell_names = json.load(f)
        self.spell_markov = MarkovGenerator(dictionary=self.spell_names, ban_list=[])

        with open(os.path.join(bot.data_dir, "markov", "spell_text.json"), encoding="utf-8") as f:
            self.spell_text = json.load(f)
        self.spell_text_markov = markovify.Text(self.spell_text, state_size=3)

        with open(os.path.join(bot.data_dir, "markov", "magic_item_text.json"), encoding="utf-8") as f:
            self.magic_item_text = json.load(f)
        # self.magic_item_text_markov = markovify.NewlineText(self.magic_item_text, state_size=3)
        self.magic_item_text_markov = markovify.Text(self.magic_item_text, state_size=3)

        with open(os.path.join(bot.data_dir, "markov", "magic_item_names.json"), encoding="utf-8") as f:
            self.magic_item_names = json.load(f)
        self.magic_item_name_markov = MarkovGenerator(dictionary=self.magic_item_names, ban_list=[])


    @commands.group()
    async def markov(self, ctx):
        """Random word/sentence commands"""
        if ctx.invoked_subcommand is None:
            await ctx.send("Invalid sub-command!\n Use `{prefix}help markov` to view sub-commands".format(
                prefix=self.bot.cfg["prefix"]))

    @markov.command(aliases=["monsters"])
    async def monster(self, ctx):
        """Random monster names (may produce garbage)"""
        await ctx.send("__Monsters__\n" + "\n".join(self.monster_markov.generate_words(amount=5)))

    @markov.command(aliases=["spells"])
    async def spell(self, ctx):
        """Randomly generated spell (may produce garbage)"""
        name1 = str(self.spell_markov.generate_words(amount=1)[0])
        text = str(self.spell_text_markov.make_sentence(tries=50))
        e = Embed(title=name1, description=text, footer="Spell",
                  colour=discord.Colour.from_rgb(random.randint(0, 255), random.randint(0, 255), random.randint(0, 255)))
        e.set_footer(text="Spell")
        await ctx.send(embed=e)

    @markov.command()
    async def magicitem(self, ctx):
        """Randomly generated magic item (may produce garbage)"""
        name1 = str(self.magic_item_name_markov.generate_words(amount=1)[0])
        text = str(self.magic_item_text_markov.make_sentence(tries=50))
        e = Embed(title=name1, description=text,
                  colour=discord.Colour.from_rgb(random.randint(0, 255), random.randint(0, 255), random.randint(0, 255)))
        e.set_footer(text="Magic Item")
        await ctx.send(embed=e)


def setup(bot):
    bot.add_cog(Markov(bot))
