from copy import deepcopy
from datetime import datetime

import discord
from discord.ext import commands
from tabulate import tabulate
from tinydb import Query

from zurok import Checks


class Bugs:
    def __init__(self, bot):
        self.bot = bot

        self.bug_reports_table = self.bot.db.table("bug_reports")

        self.bug_report_channel = None

        self.emjois = {
            "one": "\u0031\u20E3",
            "two": "\u0032\u20E3",
            "three": "\u0033\u20E3",
            "four": "\u0034\u20E3",
            "five": "\u0035\u20E3",
            "six": "\u0036\u20E3",
            "in progress": "\u23F3",
            "close": "\u274C",
            "fixed": "\u2705",
            "archive": "\U0001F5C4",
            "clarification": "\u2753",
            "spam": "\u26A0",
            "duplicate": "\u2795",
            "not a bug": "\U0001F1F3",
            "reopen": "\u21A9"
        }

        self.new_report_reactions = [
            self.emjois["one"],
            self.emjois["two"],
            self.emjois["three"],
            self.emjois["four"],
            self.emjois["five"],
            self.emjois["six"],
            self.emjois["in progress"],
            self.emjois["close"]
        ]

        self.closed_report_reactions = [
            self.emjois["clarification"],
            self.emjois["spam"],
            self.emjois["not a bug"],
            self.emjois["duplicate"],
            self.emjois["reopen"],
            self.emjois["archive"]
        ]

        self.fixed_report_reactions = [
            self.emjois["reopen"],
            self.emjois["archive"]
        ]

        self.status_colours = {
            "Open": discord.Colour.blue(),
            "Closed": discord.Colour.light_grey(),
            "Closed - Spam": discord.Colour.dark_grey(),
            "Closed - Needs Clarification": discord.Colour.dark_blue(),
            "Closed - Not a Bug": discord.Colour.purple(),
            "Closed - Duplicate": discord.Colour.dark_teal(),
            "In Progress": discord.Colour.dark_orange(),
            "Fixed": discord.Colour.green()
        }

        self.reation_functions = {
            self.emjois["one"]: {"func": self.set_severity, "args": {"severity": 1}},
            self.emjois["two"]: {"func": self.set_severity, "args": {"severity": 2}},
            self.emjois["three"]: {"func": self.set_severity, "args": {"severity": 3}},
            self.emjois["four"]: {"func": self.set_severity, "args": {"severity": 4}},
            self.emjois["five"]: {"func": self.set_severity, "args": {"severity": 5}},
            self.emjois["six"]: {"func": self.set_severity, "args": {"severity": 6}},
            self.emjois["in progress"]: {"func": self.set_status_in_progress, "args": {}},
            self.emjois["close"]: {"func": self.set_status_closed, "args": {}},
            self.emjois["fixed"]: {"func": self.set_status_fixed, "args": {}},
            self.emjois["archive"]: {"func": self.archive_report, "args": {}},
            self.emjois["clarification"]: {"func": self.close_reason, "args": {"reason": "Needs Clarification"}},
            self.emjois["spam"]: {"func": self.close_reason, "args": {"reason": "Spam"}},
            self.emjois["not a bug"]: {"func": self.close_reason, "args": {"reason": "Not a Bug"}},
            self.emjois["duplicate"]: {"func": self.close_reason, "args": {"reason": "Duplicate"}},
            self.emjois["reopen"]: {"func": self.reopen_report, "args": {}}
        }

        self.datefmt = "%d-%m-%Y %H:%M:%S"

        self.bug_report_template = {
            "user": {"name": "", "id": 0},
            "guild": {"name": "", "id": 0},
            "channel": {"name": "", "id": 0},
            "bug id": 0,
            "description": "",
            "dt": "",
            "fixed": False,
            "severity": None,
            "status": "Open",
            "report message": {"id": 0, "guild": 0},
            "rewarded": False,
            "duplicates": []
        }

        self.bounty_headers = ["S", "Type", "Reward"]
        self.bounty_table = [
            [1, "Typos, Spelling, etc.", 15],
            [2, "Bugged Command", 30],
            [3, "Broken/Unusable Command", 45],
            [4, "Exploits", 60],
            [5, "Crashes the Bot", 100],
            [6, "Crashes the Bot's Server", 200]
        ]

        self.bug_report_users = self.bot.db.table("bug_report_users")

        self.bug_user = {
            "id": 0,
            "reports": {
                "submitted": 0,
                "spam": 0,
                "duplicate": 0,
                "not a bug": 0,
                "clarification": 0,
                "list": []
            },
            "notification opt out": False,
            "blacklisted": False
        }

        self.spam_limit = 10

    async def on_ready(self):
        self.bug_report_channel = self.bot.get_channel(self.bot.cfg["bug_ch"])

    @commands.group(aliases=["bug"])
    async def bugs(self, ctx):
        """Bug reporting commands"""
        if not self.bug_report_users.contains(Query()["id"] == ctx.author.id):
            user = deepcopy(self.bug_user)
            user["id"] = ctx.author.id
            self.bug_report_users.insert(user)
            self.bot.logger.info("Added {} to bug user database".format(ctx.author.name))
        # else:
        #     user = self.bug_report_users.get(Query()["id"] == ctx.author.id)
        #     if user["reports"]["spam"] >= self.spam_limit:
        #         user["blacklisted"] = True
        #     self.bug_report_users.update(user, Query()["id"] == ctx.author.id)

        if ctx.invoked_subcommand is None:
            await ctx.send("Invalid sub-command!\n Use `{prefix}help bugs` to view sub-commands".format(
                prefix=self.bot.cfg["prefix"]))

    @bugs.command()
    async def join(self, ctx):
        """View the rewards for each level of bug"""
        await ctx.send("https://discord.gg/4vHnTYP")

    @bugs.command()
    async def bounty(self, ctx):
        """View the rewards for each level of bug"""
        await ctx.send("```" + tabulate(self.bounty_table, headers=self.bounty_headers, tablefmt="pipe") + "```")

    @bugs.command()
    async def view(self, ctx, bug_id=None):
        """View a bug report"""
        if bug_id is None:
            await ctx.send("You need to provide a bug report number/ID, in order to view a bug!")
            return

        if not bug_id.isdigit():
            await ctx.send("The bug report number/ID needs to be a whole number!")
            return

        bug_id = int(bug_id)
        if self.bug_reports_table.contains(Query()["bug id"] == bug_id):
            bug_data = self.bug_reports_table.get(Query()["bug id"] == bug_id)
            embed = await self.create_bug_report_embed(bug_data)
            await ctx.send(embed=embed)
        else:
            await ctx.send("No bug report found with number/ID {}".format(bug_id))

    @bugs.command(aliases=["dupe", "duplicates", "dupes"])
    @Checks.super_cmd()
    async def duplicate(self, ctx, *bug_ids):
        """Mark reports as duplicates of each other"""
        bug_ids = list(bug_ids)
        not_found = []

        if len(bug_ids) < 2:
            await ctx.send("Please provide more than one ID to mark as duplicates")

        for i, b in enumerate(bug_ids):
            if b.isdigit():
                bug_ids[i] = int(b)
            else:
                await ctx.send("Bug IDs need to be numbers!")
                return

            if not self.bug_reports_table.contains(Query()["bug id"] == bug_ids[i]):
                not_found.append(str(bug_ids[i]))

        if len(not_found) > 0:
            await ctx.send("Could not find bug reports with the following ID(s)\n`" + "`  `".join(not_found) + "`")
            return

        for b in bug_ids:
            if self.bug_reports_table.contains(Query()["bug id"] == b):
                bug_data = self.bug_reports_table.get(Query()["bug id"] == b)
                bug_data["duplicates"].extend(bug_ids)
                bug_data["duplicates"] = list(set(bug_data["duplicates"]))
                self.bug_reports_table.update(bug_data, Query()["bug id"] == b)

        await ctx.send("Marked bug ID as duplicates of each other\n`" + "`  `".join([str(x) for x in bug_ids]) + "`")
        self.bot.logger.info("Bugs {} marked as duplicates".format(", ".join([str(x) for x in bug_ids])))

    @bugs.group()
    @Checks.super_cmd()
    async def admin(self, ctx):
        if ctx.invoked_subcommand is None:
            await ctx.send("Invalid sub-command!\n Use `{prefix}help bugs admin` to view sub-commands".format(
                prefix=self.bot.cfg["prefix"]))

    @admin.command()
    async def guide(self, ctx):
        table = [[x, self.emjois[x]] for x in self.emjois]

        await ctx.send("Report reaction guide\n" + tabulate(table, tablefmt="plain"))

    @admin.command()
    async def blacklist(self, ctx, user):
        uid = user.strip("<@!>")
        if uid.isdigit():
            uid = int(uid)
            user = ctx.guild.get_member(uid)
        if not (isinstance(user, discord.Member) or isinstance(user, discord.User)):
            await ctx.send("First argument needs to be an @mention!")
            return

        user_data = self.bug_report_users.get(Query()["id"] == user.id)

        if user_data["blacklisted"]:
            await ctx.send("{} is already blacklisted".format(user.name))
            return

        user_data["blacklisted"] = True
        self.bug_report_users.update(user_data, Query()["id"] == user.id)
        await ctx.send("{} has been blacklisted".format(user.name))

        self.bot.logger.info("Blacklisted {} from submitting bug reports, by {}".format(user.name, ctx.author.name))

    @admin.command()
    async def unblacklist(self, ctx, user):
        uid = user.strip("<@!>")
        if uid.isdigit():
            uid = int(uid)
            user = ctx.guild.get_member(uid)
        if not (isinstance(user, discord.Member) or isinstance(user, discord.User)):
            await ctx.send("First argument needs to be an @mention!")
            return

        user_data = self.bug_report_users.get(Query()["id"] == user.id)

        if not user_data["blacklisted"]:
            await ctx.send("{} is not already blacklisted".format(user.name))
            return

        user_data["blacklisted"] = False
        self.bug_report_users.update(user_data, Query()["id"] == user.id)
        await ctx.send("{} has been un-blacklisted".format(user.name))

        self.bot.logger.info("Un-blacklisted {} from submitting bug reports, by {}".format(user.name, ctx.author.name))

    @bugs.group()
    async def notifications(self, ctx):
        """Bug report notification commands"""
        if ctx.invoked_subcommand is None:
            await ctx.send("Invalid sub-command!\n Use `{prefix}help bugs notifications` to view sub-commands".format(
                prefix=self.bot.cfg["prefix"]))

    @notifications.command(aliases=["stop", "no"])
    async def optout(self, ctx):
        """Stop the bot messaging you about any updates to your bug reports"""
        user_data = self.bug_report_users.get(Query()["id"] == ctx.author.id)

        if user_data["notification opt out"]:
            await ctx.send("You have already stopped notifications for your bug reports")
            return

        user_data["notification opt out"] = True
        self.bug_report_users.update(user_data, Query()["id"] == ctx.author.id)
        await ctx.send("You will no longer receive notifications for your bug reports")

        self.bot.logger.info("{} opted out of bug report notifications".format(ctx.author.name))

    @notifications.command(aliases=["allow", "yes"])
    async def optin(self, ctx):
        """Allow the bot to PM you about any updates to your bug reports"""
        user_data = self.bug_report_users.get(Query()["id"] == ctx.author.id)

        if not user_data["notification opt out"]:
            await ctx.send("You are already receiving notifications for your bug reports")
            return

        user_data["notification opt out"] = False
        self.bug_report_users.update(user_data, Query()["id"] == ctx.author.id)
        await ctx.send("You will now receive notifications for your bug reports")

        self.bot.logger.info("{} opted in to bug report notifications".format(ctx.author.name))

    @bugs.command()
    async def report(self, ctx, *, description: str):
        """Report any bugs with this command"""
        user = self.bug_report_users.get(Query()["id"] == ctx.author.id)
        if user["blacklisted"]:
            return
        try:
            new_bug_report = deepcopy(self.bug_report_template)
            bug_id = len(self.bug_reports_table) + 1

            new_bug_report["bug id"] = bug_id
            new_bug_report["dt"] = datetime.now().strftime(self.datefmt)

            new_bug_report["user"]["name"] = ctx.author.name
            new_bug_report["user"]["id"] = ctx.author.id

            new_bug_report["channel"]["name"] = ctx.channel.name
            new_bug_report["channel"]["id"] = ctx.channel.id

            new_bug_report["guild"]["name"] = ctx.guild.name
            new_bug_report["guild"]["id"] = ctx.guild.id

            new_bug_report["description"] = description

            report_embed = await self.create_bug_report_embed(new_bug_report)

            bug_report_channel = self.bot.get_channel(self.bot.cfg["bug_ch"])

            report_msg = await bug_report_channel.send(embed=report_embed)

            new_bug_report["report message"]["id"] = report_msg.id
            new_bug_report["report message"]["guild"] = report_msg.guild.id

            for r in self.new_report_reactions:
                await report_msg.add_reaction(r)

        except Exception as e:
            self.bot.logger.error("Error submitting bug report")
            self.bot.logger.error(e)
            await ctx.send("There was a problem submitting your bug report, please try again later.\n"
                           "If the problem persists, please notify the bot manager")
        else:
            self.bug_reports_table.insert(new_bug_report)

            if user["notification opt out"]:
                await ctx.send("Thank you, bug report #**{}** has been submitted".format(bug_id))

            else:
                await ctx.send("Thank you, bug report #**{}** has been submitted."
                               "You will be notified of any updates\n"
                               "Don't want to be notified? "
                               "Use `{}bugs notifications stop`".format(bug_id, self.bot.cfg["prefix"]))

                self.bot.logger.info("New bug submitted - #{}".format(bug_id))

            user["reports"]["submitted"] += 1
            self.bug_report_users.update(user, Query()["id"] == ctx.author.id)

    # async def on_reaction_add(self, reaction, user):
    async def on_raw_reaction_add(self, emoji, message_id, channel_id, user_id):
        if channel_id != self.bug_report_channel.id:
            return

        msg = await self.bug_report_channel.get_message(message_id)
        user = msg.guild.get_member(user_id)

        if not user.bot:
            if user.id != self.bot.cfg["owner"]:
                return

            if self.bug_reports_table.contains(Query()["report message"]["id"] == msg.id):
                bug_data = self.bug_reports_table.get(Query()["report message"]["id"] == msg.id)

                if emoji.name in self.reation_functions:
                    func = self.reation_functions[emoji.name]["func"]
                    args = self.reation_functions[emoji.name]["args"]
                    await func(bug_data, **args)

                try:
                    await msg.remove_reaction(emoji, user)
                except discord.errors.NotFound:
                    pass

    async def set_severity(self, bug_data, severity):
        bug_data["severity"] = severity
        self.bug_reports_table.update(bug_data, Query()["bug id"] == bug_data["bug id"])
        await self.update_report_message(bug_data)

        self.bot.logger.info("Bug {} severity set to {}".format(bug_data["bug id"], severity))

    async def set_status_closed(self, bug_data):
        bug_data["status"] = "Closed"

        msg = await self.bug_report_channel.get_message(bug_data["report message"]["id"])
        await msg.clear_reactions()
        for r in self.closed_report_reactions:
            await msg.add_reaction(r)

        self.bug_reports_table.update(bug_data, Query()["bug id"] == bug_data["bug id"])
        await self.update_report_message(bug_data)

        self.bot.logger.info("Bug {} closed".format(bug_data["bug id"]))

    async def set_status_in_progress(self, bug_data):
        bug_data["status"] = "In Progress"
        self.bug_reports_table.update(bug_data, Query()["bug id"] == bug_data["bug id"])
        await self.update_report_message(bug_data)

        user = self.bot.get_user(bug_data["user"]["id"])
        user_data = self.bug_report_users.get(Query()["id"] == user.id)
        if not user_data["notification opt out"]:
            await user.send("Status update for bug report #**{}** - **In Progress**".format(bug_data["bug id"]))
        self.bot.logger.info("Bug {} in progress".format(bug_data["bug id"]))

    async def set_status_fixed(self, bug_data):
        bug_data["status"] = "Fixed"

        msg = await self.bug_report_channel.get_message(bug_data["report message"]["id"])
        await msg.clear_reactions()
        for r in self.fixed_report_reactions:
            await msg.add_reaction(r)

        user = self.bot.get_user(bug_data["user"]["id"])
        user_data = self.bug_report_users.get(Query()["id"] == user.id)

        if not bug_data["rewarded"]:
            severity = bug_data["severity"]
            points = self.bounty_table[severity - 1][2]
            guild = self.bug_report_channel.guild

            if self.bot.reward_user is None:
                await user.send("Your bug report #**{}**, has been marked as fixed and will be in the next update.\n"
                                "There was a problem delivery your reward, this has been logged and will be addressed asap!"
                                .format(bug_data["bug id"]))

                await self.bot.owner.send("Error sending {0.name} {0.id} their **{1}** reward for bug #{2}".format(
                    user, points, bug_data["bug id"]))
                self.bot.logger.error("Error sending {0.name} {0.id} their **{1}** reward for bug #{2}".format(
                    user, points, bug_data["bug id"]))
                return

            await self.bot.reward_user(user, points, self.bug_report_channel, guild)
            bug_data["rewarded"] = True

            if not user_data["notification opt out"]:
                await user.send("Your bug report #**{}**, has been marked as fixed and will be in the next update.\n"
                                "You have been rewarded **{}** points for reporting the issue"
                                .format(bug_data["bug id"], points))
            self.bot.logger.info("Bug {} marked as fixed and {} issued {}pts reward".format(bug_data["bug id"], user.name, points))
        else:
            if not user_data["notification opt out"]:
                await user.send("Your bug report #**{}**, has been marked as fixed and will be in the next update".format(bug_data["bug id"]))
            self.bot.logger.info("Bug {} marked as fixed".format(bug_data["bug id"]))

        self.bug_reports_table.update(bug_data, Query()["bug id"] == bug_data["bug id"])
        await self.update_report_message(bug_data)

    async def update_report_message(self, bug_data):
        msg = await self.bug_report_channel.get_message(bug_data["report message"]["id"])
        embed = await self.create_bug_report_embed(bug_data)
        await msg.edit(embed=embed)

        if bug_data["severity"] is not None and "Closed" not in bug_data["status"] and bug_data["status"] != "Fixed":
            await msg.add_reaction(self.emjois["fixed"])

    async def archive_report(self, bug_data):
        msg = await self.bug_report_channel.get_message(bug_data["report message"]["id"])
        await msg.delete()
        self.bot.logger.info("Bug {} archived".format(bug_data["bug id"]))

    async def close_reason(self, bug_data, reason):
        bug_data["status"] = "Closed - " + reason

        msg = await self.bug_report_channel.get_message(bug_data["report message"]["id"])
        await msg.clear_reactions()
        for r in self.fixed_report_reactions:
            await msg.add_reaction(r)

        self.bug_reports_table.update(bug_data, Query()["bug id"] == bug_data["bug id"])
        await self.update_report_message(bug_data)

        user = self.bot.get_user(bug_data["user"]["id"])
        user_data = self.bug_report_users.get(Query()["id"] == user.id)

        if not user_data["notification opt out"]:
            await user.send("Status update for bug report #**{}** - "
                            "**Closed**\nReason: **{}**".format(bug_data["bug id"], reason))

        user_data["reports"][reason.lower()] += 1
        self.bug_report_users.update(user_data, Query()["id"] == user.id)
        self.bot.logger.info("Bug {} closed - {}".format(bug_data["bug id"], reason))

    async def reopen_report(self, bug_data):
        bug_data["status"] = "Open"

        msg = await self.bug_report_channel.get_message(bug_data["report message"]["id"])
        await msg.clear_reactions()
        for r in self.new_report_reactions:
            await msg.add_reaction(r)

        self.bug_reports_table.update(bug_data, Query()["bug id"] == bug_data["bug id"])
        await self.update_report_message(bug_data)

        self.bot.logger.info("Bug {} reopened".format(bug_data["bug id"]))

    async def create_bug_report_embed(self, bug_data):
        bug_embed = discord.Embed()

        bug_embed.description = bug_data["description"]
        bug_embed.title = "Bug report #{}".format(bug_data["bug id"])
        bug_embed.add_field(name="Opened", value=bug_data["dt"], inline=True)
        bug_embed.add_field(name="Severity", value=bug_data["severity"], inline=True)
        bug_embed.add_field(name="Status", value=bug_data["status"], inline=True)

        bug_embed.add_field(name="User", value=bug_data["user"]["name"], inline=True)
        bug_embed.add_field(name="Channel", value=bug_data["channel"]["name"], inline=True)
        bug_embed.add_field(name="Guild", value=bug_data["guild"]["name"], inline=True)

        if len(bug_data["duplicates"]) > 0:
            d_list = [str(x) for x in bug_data["duplicates"] if x != bug_data["bug id"]]
            bug_embed.add_field(name="Duplicates", value=", ".join(d_list), inline=False)

        bug_embed.set_footer(text="Last update")
        bug_embed.timestamp = datetime.now()

        bug_embed.colour = self.status_colours[bug_data["status"]]
        return bug_embed


def setup(bot):
    bot.add_cog(Bugs(bot))
