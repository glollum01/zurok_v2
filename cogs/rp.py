import _string
import os
import random

from discord.ext import commands
from tinydb import TinyDB, Query


class RP:
    def __init__(self, bot):
        self.bot = bot

        self.bot.rp_data = RPData(bot)
        self.data = RPData(bot)
        self.gformat = GoalFormatter(self.data)

        self.setting_str = \
            "**Setting**\n*Location*: {0.name}\n*Weather*: {0.weather}\n*Time*: {0.time}\n*Group Goal*: {0.group_goal}"
        self.char_str = \
            "**{0.mention}**\n*Name*: {1.name}\n*Race*: {1.race}\n*Role*: {1.role}\n*Height*: {1.height}\n" \
            "*Notable features*: {1.features}"
        self.dm_char_str = \
            "*Name*: {0.name}\n*Race*: {0.race}\n*Role*: {0.role}" \
            "\n*Height*: {0.height}, *Weight*: {0.weight} lbs, *Age*: {0.age}" \
            "\n*Secret Goal*: {0.goal}\n*Notable features*: {0.features}"

    @commands.command()
    @commands.guild_only()
    async def rp(self, ctx, *players):
        """Generates a simple setting and characters for 2 or more players"""
        players = ctx.message.mentions

        if len(players) < 2:
            await ctx.send("2 or more players are needed!")
        else:
            # Setting
            setting = Location(self.data, self.gformat, players)
            setting_str = self.setting_str.format(setting)
            await ctx.send(setting_str)

            for x, player in enumerate(players):
                character = Character(self.data, self.gformat, [p for y, p in enumerate(players) if y != x])
                await ctx.send(self.char_str.format(player, character))
                if not player.bot:
                    await player.send(self.dm_char_str.format(character))


class GoalFormatter:
    def __init__(self, data):
        self.data = data

        self.funcs = {}

        for method in dir(self):
            if "_" not in method:
                if callable(getattr(self, method)):
                    self.funcs[method] = getattr(self, method)

    def x(self):
        return random.randint(2, 10)

    def y(self):
        return random.randint(10, 100)

    def z(self):
        return random.randint(100, 1000)

    def animal(self):
        return random.choice(self.data.creatures.all())["str"]

    def material(self):
        return random.choice(self.data.materials.all())["str"]

    def supplies(self):
        return random.choice(self.data.supplies.all())["str"]

    def inn(self):
        return random.choice(self.data.inns.all())["str"]

    def contest(self):
        return random.choice(self.data.contests.all())["str"]

    def item(self):
        return random.choice(self.data.items.all())["str"]

    def meat(self):
        return random.choice(self.data.meats.all())["str"].lower()

    def mad(self):
        return random.choice(self.data.voices.all())["str"]

    def time(self):
        return random.randint(1, 12)

    def creature(self):
        return random.choice(self.data.creatures.all())["str"]

    def colour(self):
        return random.choice(self.data.colours.all())["str"]

    def valuables(self):
        return random.choice(self.data.valuables.all())["str"]

    def format_(self, string: str, players):
        missing = [fname for _, fname, _, _ in _string.formatter_parser(string) if fname]

        vals = {}
        for m in missing:
            if "|" in m:
                vals[m] = random.choice(m.split("|"))
            elif m == "target":
                vals[m] = random.choice(players).mention
            elif m == "target2":
                vals[m] = random.choice(["the gods"].extend([p.mention for p in players]))
            else:
                vals[m] = self.funcs[m]()

        return string.format(**vals)


class RPData:
    def __init__(self, bot):
        bot.logger.info("rp: Loading RP Data...")

        self.DATA_DIR = os.path.join(bot.data_dir, "rp")
        if not os.path.isdir(self.DATA_DIR):
            os.makedirs(self.DATA_DIR)

        self.DB = TinyDB(self.DATA_DIR + "/rp_data_db.json")

        self.names = self.DB.table("names")

        self.locations = self.DB.table("locations")
        self.times = self.DB.table("times")
        self.weathers = self.DB.table("weathers")

        self.roles = self.DB.table("roles")
        self.races = self.DB.table("races")

        self.goals = self.DB.table("goals")
        self.group_goals = self.DB.table("group_goals")

        self.items = self.DB.table("items")
        self.magic_items = self.DB.table("magic_items")
        self.spells_simple = self.DB.table("spells_simple")

        self.meats = self.DB.table("meats")
        self.voices = self.DB.table("voices")
        self.animals = self.DB.table("animals")
        self.contests = self.DB.table("contests")

        self.inns = self.DB.table("inns")
        self.bandits = self.DB.table("bandits")
        self.creatures = self.DB.table("creatures")

        self.materials = self.DB.table("materials")
        self.supplies = self.DB.table("supplies")
        self.colours = self.DB.table("colours")

        self.hurts = self.DB.table("hurts")
        self.injuries = self.DB.table("injuries")
        self.notable_features = self.DB.table("notable_features")

        self.valuables = self.DB.table("valuables")
        self.trinkets = self.DB.table("trinkets")
        self.monsters = self.DB.table("monsters")
        self.spells = self.DB.table("spells")

        bot.logger.info("rp: Loaded RP Data.")


class Location:
    def __init__(self, data, gformat, players):
        self._loc = random.choice(data.locations.all())
        self.name = self._loc["name"]
        self.time = random.choice(data.times.all())["str"]
        self.weather = data.weathers.get(Query().index == random.choice(self._loc["weathers"]))["str"]
        self.group_goal = gformat.format_(random.choice(data.group_goals.all())["str"], players)


class Character:
    def __init__(self, data, gformat, players):
        # role data
        self._role = random.choice(data.roles.all())
        # role name
        self.role = self._role["name"]

        self.goal = data.goals.get(Query().index == random.choice(self._role["goals"]))["str"]
        self.goal = gformat.format_(self.goal, players)

        self.forename = random.choice(data.names.search(Query().forename == True))["str"]
        self.surname = random.choice(data.names.search(Query().surname == True))["str"]
        self.name = self.forename + " " + self.surname

        # race data
        self._race = random.choice(data.races.all())
        # race name
        self.race = self._race["name"]

        self.age = random.randrange(self._race["age"]["min"], self._race["age"]["max"])
        # int of height
        self._height = random.randrange(self._race["height"]["min"], self._race["height"]["max"])
        # atr of height in ft\in
        self.height = str(int((self._height - (self._height % 12)) / 12)) + "' " + str(self._height % 12) + "\""
        # int of height in lbs
        self.weight = random.randrange(self._race["weight"]["min"], self._race["weight"]["max"])

        self.items = []

        # noteable features
        self.features = [x["str"] for x in random.sample(data.notable_features.all(), k=random.randint(1, 3))]
        self.features = ", ".join([gformat.format_(x, players).title() for x in self.features])

    def __str__(self):
        return str({
            "race": self.race,
            "role": self.role,
            "age": self.age,
            "height": self.height,
            "weight": self.weight,
            "name": self.name,
            "items": self.items,
            "goal": self.goal,
            "features": self.features
        })


def setup(bot):
    bot.add_cog(RP(bot))
