from discord.ext import commands
from tabulate import tabulate
from tinydb import Query


class Stats:
    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    async def stats(self, ctx, cmd=None):
        """Displays usage stats for a command"""
        if cmd is not None:
            if cmd not in self.bot.all_commands:
                await ctx.send("Not a valid command")
            else:
                cmd_stats = self.get_cmd_stats(ctx.guild, cmd)
                await ctx.send("**{}**\n*Total:* {}\n*This Guild:* {}".format(
                    cmd, cmd_stats["total"], cmd_stats[str(ctx.guild.id)]))
        else:
            sorted_cmds = sorted([self.get_cmd_stats(ctx.guild, cmd) for cmd in self.bot.all_commands if not self.bot.get_command(cmd).hidden],
                                 key=lambda k: k["total"], reverse=True)
            sorted_cmds = [cmd for cmd in sorted_cmds if cmd["total"] > 0]
            _ = []
            for k in sorted_cmds:
                if k["name"] in ["stats"]:
                    continue
                _.append([k["name"], k["total"], k[str(ctx.guild.id)]])

            await ctx.send("**Command Stats**\n```"
                           + tabulate(_, headers=["Command", "Total", "This Guild"], tablefmt="pipe") + "```")

    def get_cmd_stats(self, guild, cmd):
        if self.bot.stats_table.contains(Query().name == cmd):
            cmd_stats = self.bot.stats_table.get(Query().name == cmd)
            if str(guild.id) not in cmd_stats:
                cmd_stats[str(guild.id)] = 0
        else:
            cmd_stats = {"name": cmd, str(guild.id): 0, "total": 0}
            self.bot.stats_table.insert(cmd_stats)

        return cmd_stats


def setup(bot):
    bot.add_cog(Stats(bot))
