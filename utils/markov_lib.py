import random


class markov:
    # key: character
    # value: dict of weights [ next key ] = weight
    # starting state has key ''
    # ending state has key '\0' (not actually stored in states)

    def __init__(self, maxkey=1):
        self.states = {}
        self.maxkey = maxkey

    def digest(self, word):
        maxkey = min(self.maxkey, len(word))
        if maxkey < 1:
            raise Exception('undigestable')

        self.update_state('', word[0])

        for inext in range(1, len(word)):
            for ikey in range(max(0, inext - maxkey), inext):
                self.update_state(word[ikey: inext], word[inext])

        for keylen in range(1, maxkey + 1):
            self.update_state(word[len(word) - keylen:], '\0')

    def prune(self):
        """remove states with long keys that have few edges"""
        reject_keys = set()
        for key, edges in self.states.items():
            if len(key) > 1 and len(edges) < 3:
                reject_keys.add(key)
        for key in reject_keys:
            del self.states[key]

    def report(self):
        keydist = {}
        for key, edges in self.states.items():
            if len(key) not in keydist:
                keydist[len(key)] = 0
            keydist[len(key)] += 1

    def update_state(self, key, next):

        if next == '' or key == '\0':
            raise Exception('invalid update_state')
        if key not in self.states:
            self.states[key] = {}
        state = self.states[key]
        if next not in state:
            state[next] = 0
        state[next] += 1

    def get_next(self, word):
        key = ''
        for keylen in range(min(self.maxkey, len(word)), 0, -1):
            subword = word[len(word) - keylen:]
            if subword in self.states:
                key = subword
                break
        state = self.states[key]
        weightsum = sum([weight for next, weight in state.items()])
        r = random.randint(1, weightsum)
        for next, weight in state.items():
            if r <= weight:
                return next
            else:
                r -= weight
        raise Exception('freaky get next')

    def gen_word(self):
        word = ''
        next = self.get_next(word)
        while next != '\0':
            word += next
            next = self.get_next(word)
        return word

    def __str__(self):
        s = ''
        for key, state in self.states.items():
            s += key + ': ' + str(state) + '\n'
        return s


class LCSList:
    def __init__(self, words=[]):
        self.substrings = set()
        self.addwords(words)

    def addwords(self, words=[]):
        for word in words:
            for k in range(1, len(word) + 1):
                self.substrings |= frozenset([word[i:i + k] for i in range(len(word) - k + 1)])

    def lcs_len(self, word):
        for k in range(1, len(word) + 1):
            hit = False
            for i in range(len(word) - k + 1):
                if word[i:i + k] in self.substrings:
                    hit = True
                    break
            if not hit:
                return k - 1
        return len(word)


def most_similar(w, sourcewords, minsim=3):
    mostsim = 0
    similars = []
    for s in sourcewords:
        x = LCSList.lcs_len(w, s)
        if x >= minsim:
            if x > mostsim:
                mostsim = x
                similars = []
            if x == mostsim:
                similars.append(s)
    return similars


class MarkovGenerator:
    def __init__(self, dictionary: list, ban_list: list=None, state: dict=None):
        self.markov = markov(6)

        self.sourcewords = dictionary

        self.lcs = LCSList(self.sourcewords)

        self.ban_list = ["The &\n", "The\n", "The \n", "The The\n", "The The \n", "The & \n"]
        if ban_list is not None:
            self.ban_list.extend(ban_list)

        if state is None:
            self.create_state()
        else:
            self.set_state(state)

    def create_state(self):
        self.markov.states = {}
        for word in self.sourcewords:
            self.markov.digest(word)
        self.markov.report()
        self.markov.prune()
        self.markov.report()

    def get_state(self):
        return self.markov.states

    def set_state(self, state):
        self.markov.states = state

    def generate_words(self, amount=20):
        words = []
        while len(words) < amount:
            w = self.markov.gen_word()
            lcslen = self.lcs.lcs_len(w)
            if w not in self.sourcewords and lcslen < len(w) and w not in self.ban_list:
                words.append(w)

        return words
