class BotRemote:
    def __init__(self, bot, *, update_func=None, restart_func=None):
        self.bot = bot

        self.restart_func = restart_func
        self.update_func = update_func

    async def send(self, channel: int, msg: str, delete_after: int=0):
        # dont use this, its dumb
        await self.bot.get_channel(channel).send(msg=msg, delete_after=delete_after)

    async def get_cfg(self):
        # exclude some values
        return self.bot.cfg

    async def set_cfg(self, cfg):
        # check against schema
        pass

    async def restart(self):
        if self.restart_func is not None:
            await self.restart_func()

    async def update(self):
        if self.update_func is not None:
            await self.update_func()
